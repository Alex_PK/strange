# 0.10

- **[DEV BREAKING]** Strange now requires Rust 1.31 (2018 edition) to compile
- **[POSSIBLE BREAKING]** Updated all template engines. Some templates could stop working.
- Updated handbrake to 1.x
- Updated tera to 0.11
- Updated pulldown to 0.2
- Removed dependency on error-chain
- Removed dependency on hyper/tokio/futures by using simple-server
- Updated other dependencies


# 0.9.0

- **[BREAKING]** The `template` meta-data in pages is now `type`
- **[EXPERIMENTAL]** Support for themes
- **[BREAKING]** Templates in the `tpl` directory are not used anymore, and existing ones are ignored
- **[BREAKING]** `strange new` now takes an optional prefix with `-p`. Dest path now needs `-P` (capital p)
- Added Type plugin for grouping pages by type
- Can run `strange build` from any subdirectory inside the project
- Can use `strange build project-dir` (and `watch` and `serve`) from outside the project to build it
- Add several filters, checkers and functions for Tera-based templates
- Documentation is now in a separate, buildable, deployable directory (docs)
- `strange new` will now create all the directories in the path if they don't exist
- `strange config update` now creates a backup (only if the content changes)
- New `redirect` plugin can generate an `.htaccess` file with 30x redirects from old URLs
- Choose page template by using the final file extension instead of trying to guess it
- Add more date formats
- Improved watch and serve file-change response speed
- Fix RSS generation
- Add RSS option to select page types to include in the feed
- Add RSS option to use a custom title for the feed
- Switch to hyper 0.11 for the `serve` command
- Fix some log levels
- Bug fixes


# 0.8.1

- Published on crates.io


# 0.8.0

- Complete overhaul of rendering engine to allow modular multi-engine rendering
