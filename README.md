# Strange

Strange is a static site generator.

It allows you to model your website freely, while helping by converting commonmark (standardized markdown) files
into HTML pages, applying themes, providing helpers for the templates, and other little things.

## Download

You can find [strange on crates.io](https://crates.io/crates/strange).

If you have a recent version of Rust installed (1.18) you can install it with:

```bash
cargo install strange
```

The binary will be created in `~/.cargo/bin/strange`.

Or you can download a pre-compiled binary for Linux/amd64 from the `Downloads` section on bitbucket.

## Documentation

The documentation is available in the [Docs](docs/src/index.md) directory.

## Backwards compatibility

This version of strange introduces some breaking changes.

Please check the [Changelog](CHANGELOG.md) for an overview.


## LICENSE

Strange is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 2.

Strange is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Strange.  If not, see <http://www.gnu.org/licenses/>.

