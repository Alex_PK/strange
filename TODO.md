## TODO

- [ ] Find a way to have absolute links in plugins
- [ ] permalink/id
- [ ] Atom file generation
- [ ] list of articles by date/year/month
- [ ] Add support for other template engines
- [ ] Configurable date and time format for filenames
- [ ] Maybe get rid of hyper for local serving
