---
title: Introduction
date: 2017-11-13 09:03:57
type: page
---
# Introduction

This is a brief tutorial of the basic workings of `strange`.

Move into the directory you want to create your new project in. For example, let's say you want to create a new
site called `My Example` in a directory called `example` inside `$HOME/Projects`. Here is an example with the expected
output

```bash
$ cd $HOME/Projects

$ strange init example
 INFO Application started
 INFO Project initialized

$ cd example
```

A few items will be created for you. This should be the structure you should find:

```bash
$ tree -F
.
├── build/
├── data/
├── src/
├── strange.yaml
└── themes/
```

Here is what they are:

- `strange.yaml` is the configuration file for the project. It must be in the root of your project.
- `src/` will contain all your website in source form
- `build/` is where the generated site will be stored
- `themes/` contains the themes you can download or create
- `data/` is used by the `data` plugin. More on this later.

## Your first page

From anywhere inside the project you can run `strange` with any command (except `init`, which would create a
sub-website and a lot of confusion :)

To create a new page you can manually create a file with your editor or use the command:

```bash
$ strange new My first page
$ strange new My first page
 INFO Application started
 INFO Page created: `my-first-page.md`.
```

This will create a new file `src/my-first-page.md` with a default prelude:

```bash
$ cat src/my-first-page.md 
---
title: My first page
date: 2017-11-13 09:24:20
type: page
---
# My first page

```

The prelude is a piece of YAML text enclosed between two `---`, before your real page content, and can be used to
provide meta-information to the rendering engine. In this case only the title (used in the `<title>` HTML element,
for example), the date (which you can use in your template) and the type of document (the default is `page`. It can be
used to selct the template to apply, for example) are provided.

Please see the chapter on the [Prelude](#TODO) for more info.


## Generating the website

Once you are done creating and editing your pages, you can generate the final website:

```bash
$ strange build
$ strange build
 INFO Application started
 INFO Building...
 INFO Found 2 files
 INFO Parsing meta-data
 INFO Processing plugins
 INFO Rendering
 INFO Post-processing plugins
 INFO Processing theme
 INFO Build finished.
```

Which will generate the pages in `build/`:

```
tree -F build
$ tree -F build
build/
├── index.html
├── my-first-page.html
└── rss.xml
```

## I'm getting errors and warnings!

As of today, you will get some warnings when running some commands. These are mainly due to the lack of a default
theme preinstalled when creating the website. This is goind to be fixed soon.
