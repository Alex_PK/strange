---
title: The config file
date: 2017-11-13 18:26:46
type: page
---
# The config file

While creating a website via `strange init`, a config file named `strange.yaml` is created in the root of the
project.

It contains configuration parameters for several parts of the process.

This is an example taken from the the test website created in the [Introduction](01-introduction.html):

```yaml
site:
  title: My strange site
  url: http://example.net
  description: 
  indexes: true

pages:
  date: false
  time: false
  author: Unknown

paths:
  src: src
  dest: build
  themes: themes

serve:
  ip: 127.0.0.1
  port: 9876

theme:
  name: default
  dest: theme
  options:
    {}
  data:
    {}

plugins:
  data:
    dir: data
  rss:
    enabled: true
    filename: rss.xml
    num: 10
```

## Config file sections

Let's go through the sections one by one.


### Site

This section contains general meta-data about the web site.

```yaml
site:
  title: My strange site
  url: http://example.net
  description: 
  indexes: true
```

`title` is the name of the website. It can be used as a variable in the theme templates, or in the RSS feed,
for example.

`url` is the URL where the website will be published. For now it can be used to build absolute URLs to your pages,
but in the future it will probably be used for other purpouses.

`description` is a free-form one-line descripion of the website. It can be used as a punch line or a motto in the
templates.

`indexes` is a flag. When set to true, `strange` will try to create index.html pages in directories that do not contain
it, by listing all the pages, directories and files contained in it. You can think of it as similar to the
+Indexes option in Apache, if you are familiar with it.

### Pages

This section defines the general configuration of the pages.

```yaml
pages:
  date: false
  time: false
  author: Unknown
```

`date` is a flag which, if set to true, will prepend the date to the file name of every page you create with
`strange new`.

`time` can only be used with `date`, and also prepends the time.

`date` and `time` are useful when managing a blog, as they help create a structure sorted by date.

`author` is the name of the author that will be used if none is specified in a page's prelude.


### Paths

This section contains the project structure configuration.

Most of the projects will be OK with the defaults, but special cases could require to rename the directories.

```yaml
paths:
  src: src
  dest: build
  themes: themes
```

`src` is the directory containing the source of your web site.

`dest` is where the built website will be stored.

`themes` is the directory containing the themes


### Serve

This section is used to configure the integrated webserver, which can be started via `strange serve`.

```yaml
serve:
  ip: 127.0.0.1
  port: 9876
```

`ip` and `port` define where the server should be listening on.

Please do not use `strange` to serve production websites. :)


### Theme

This section configures the themes.

```yaml
theme:
  name: default
  dest: theme
  options:
    {}
  data:
    {}
```

`name` determines which of the installed themes will be used during the build. It is the name of the directory
containing the theme inside the `themes/` directory (or the one configured in the `paths` section).

`dest` specifies where the theme files will be copied (inside the `build/` directory, or whichever you configured as
`dest` in the `paths` section). This is to avoid overwriting source files with theme files by mistake.

`options` and `data` are used to pass specific options and data (that will be available through template-engine
variables) to the current theme during the build, and they are specific for the theme in use.


### Plugins

This section contains configurable parameters for the included plugins.

Every sub-section is specific for the plugin it refers to. Please see the [Plugins](#TODO) chapter for the specifics.

```yaml
plugins:
  data:
    dir: data
  rss:
    enabled: true
    filename: rss.xml
    num: 10
```


## Config file management

The config file is to be edited manually, but sometimes it needs to be cleaned up, for example when a new version
of `strange` comes out adding new options.

In these cases, the command `strange config update` will read the current config file, try to keep the
user-configured options, add (or remove) options that have been changed in `strange` and overwrite it.

**WARNING**: This command does **no backups** and **completely overwrites** the config file, including comments.
Use at your own risk!
