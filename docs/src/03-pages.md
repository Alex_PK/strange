---
title: Pages
date: 2017-11-13 18:26:54
type: page
---
# Pages

Every file in the `src` directory will be copied over to the `build` directory. Some files will be copied as are,
and files with specific extensions will be processed (and possibly renamed) during the build process.

For example, files ending in `.md` will be processed by the commonmark (standardized markdown) processor and renamed
to `.html`, and files with a `twig` (or `.tera` or `.j2`) extension will be processed by Tera (a template engine
with a syntax similar to Twig and Jinja2) and the extension will be removed. 

So, a file named `my-page.md.twig` will be processed by Tera first, then by Commonmark, and saved into `my-page.html`
in the destination directory, while a file named `second-page.html.twig` will be processed by Tera and saved as
`second-page.html`.

All the files that will end up having an extension of `.html` are considered "pages".


## Supported extensions

- `.md` will parse the page with a commonmark/markdown syntax and generate a `.html` file.

- `.twig`, `.j2`, `.tera` will parse the page with Tera, a Jinja2-inspired engine, and remove the extension.

- `.hbs` will parse the page with handlebars and remove the extension


### Examples

A file not ending with an engine-supported extension will not be parsed. Any file that, after the initial parsing,
ends with a `.html` extension will be wrapped by the specified (or default, which is `page`) page template (see the
[Themes](#TODO) chapter).

- `my-post.html` **will not** be parsed, but it will be wrapped in the page template

- `my-post.twig.html` **will not** be parsed, but it will be **wrapped** in the page template

- `my-post.html.twig` **will** be parsed by tera and **wrapped** in the page template, and **renamed** `my-post.html`

- `my-post.md` **will** be parsed by markdown and **wrapped** in the page template, and **renamed** `my-post.html`

- `my-post.twig` **will** be parsed by tera, **renamed** `my-post` and **not wrapped** in a page template

- `main.css.twig` **will** be parsed by tera and **renamed** `main.css`

- `main.js.twig` **will** be parsed by tera and **renamed** `main.js`

And so on.


### Caution

If a page uses a template engine, it's highly recommended to have a `caption` in the YAML [prelude](#prelude).

If it's not present, strange will try to use the first 2 paragraph in the body to generate
a caption, and this could truncate template constructs, generating an invalid template to parse.


# Page structure

A page is coomposed of a (optional) prelude, enclosed between lines containing only `---` and encoded as YAML,
at the beginning of the file, and the real page content (everything after the second `---` line, if the
prelude is present).


## Page content

The most important part, of course, is the content of the page, so we will discuss it first.

It can be anything, and it's your responsibility to put content that makes sense, both in format and in meaning. :)

A page called `my-page.html` should contain HTML. A page called `my-page.md` should contain Commonmark.

Any kind of file can be created in the source directory: `.txt`, `.js`, `.css`, or binary files like `.jpg` or `.png`.

Some files will be ignored during the build process. Namely, everything starting with a dot (`.`) or an underscore (`_`)
and everything ending with `.bak` or a tilda (`~`) which is sometimes used in Linux for backup files.

Any file whose name will end in `.html` will be considered a page (and receive the `page` template from the theme).

Additionally, if it starts with `index.`, it  will be considered an index (and, for example, a specific `index` template will be used
from the theme)

For both of them, the template can be overriden in the [Prelude](#prelude).

Everything else will be copied as is.

Commonmark syntax can be found on the [Commonmark](#TODO) website.

Tera syntax can be found on the [Tera](#TODO) website.

Handlebars syntax can be found on the [Handlebars](#TODO) website.


## Prelude

The prelude is a small snippet of YAML at the beginning of a page, enclosed in `---`.

For example:

```markdown

---
title: My new blog post
date: 2017-03-19T12:34:56Z
author: Alex
type: post
caption: |
  A short (3-4 lines) caption of the page, to be shown as a preview
tags:
  - blogpost
  - random-thoughts
---
This is the _full text_ of the blog in **markdown** format.

It supports the commonmark specification.

Tables and footnotes extensions are enabled.

```

Supported properties are:

- `title` is the page title. It will be available as a variable to the template engine.

- `date` is the date the page was created. It must be in valid ISO8601 format and will be available in the templates.

- `author` is the author of the page. It will be available in the template, which is also responsible for possibly
parsing it (to extract the email, for example)

- `type` is the page type. It defines the template

- `caption` is the caption of the page. It can be used in an index file, or in the RSS. There is no size limit, but
it is recommended to keep it short. If it's not provided, `strange` will try to extrapolate it from the first
two paragraphs of the main content (by detecting new-lines).

- `tags` is a list (or array, if you prefer) of tags associated with the page. They will be available as an array
of strings in the templates.

- every other field here will be stored into an `extra` object, available in the templates. Some plugins could support
or require additional information. Please see each plugin's description.


# Creating new pages

To create a new page you can simply create a new file inside the source directory or any of its subdirectories
and write the content and the prelude by hand, or you can use the `strange new` command.

### `strange new`

You can get the full syntax by running `strange help new`:

```bash
$ strange help new
strange-new 
Create a new empty article with the given title.

USAGE:
    strange new [FLAGS] [OPTIONS] <TITLE>...

FLAGS:
    -d, --date    Prepend date in Y-m-d format to file name
    -h, --help    Prints help information
    -t, --time    Add time to date in filename and prelude (doesn't work without -d)

OPTIONS:
    -P, --path <PATH>        Create the article in the specified (relative) sub-path
    -p, --prefix <prefix>    Add a prefix to the file name. If date/time is requested, the prefix is added after the date

ARGS:
    <TITLE>...    Title of the page

```

The basic syntax (`strange new My page title`) will create a new file in the root of the source directory with a
"slugified" version of the title you provide as file-name, and the `.md` extension. So, in this case, `my-new-page.md`.

#### Date and time in the file name

If date (and possibly time) is enabled in the [Config](#TODO), or when using the `-d` (and possibly `-dt`) on the
command line, the file name will be prepended with date (and time). For example: `strange create -dt My new page`
will create `2017-11-14-1838-my-new-page.md`

#### Custom prefix in the file name

If, for any reason, a prefix is required in the file name, but it must not appear in the title, using `-p` will
prepend it to the file name (after the date/time). For example: `strange new -p 01 My ordered page` will create
`01-my-ordered-page.md`, while `strange new -dtp 01 My ordered page` will create `2017-11-14-1844-01-my-ordered-page.md`.

#### Creating pages in sub-directories

To create a page inside a subdirectory of the source directory, the `-P` (capital P, not to be confused with `-p`)
allows to do that: `strange new -dtP blog My new blog post` will create `blog/2017-11-14-1847-my-new-blog-post.md`.

