---
title: Building, Watching, Serving
date: 2017-11-15 09:11:51
type: page
---
# Building, Watching, Serving

To transform the source of the website into browsable content, it needs to be built. This is the purpouse of the
`strange build` command.

## Build

The syntax of the `build` command is simple:

```bash
$ strange help build
strange-build 
Builds the website.

USAGE:
    strange build [PATH]

FLAGS:
    -h, --help    Prints help information

ARGS:
    <PATH>    Build the project in the specified directory, instead of the current one
```

I the most common form, running `strange build` from anywhere inside the project will build it into the configured
destination directory.

If the command is ran outside of the project dir, a path can be provided on the command line to help `strange`
find the project.


## Watch

`watch` does exactly the same job as `build`, but keeps watching the source directory for modifications and
automatically rebuilds the website when it happens.

```bash
$ strange help watch
strange-watch 
Watches the source dir for changes and automatically rebuild the pages.

USAGE:
    strange watch [PATH]

FLAGS:
    -h, --help    Prints help information

ARGS:
    <PATH>    Build and watch the project in the specified directory, instead of the current one
```

The syntax and the options are the same as the `build` command.

The shell console will be kept locked while the command is running, and output will be printed on it during the
execution. It is recommended to dedicate a shell window to this process, or run it in background redirecting the
output, if multi-window is not available.

*WARNING*: Detecting changes on the filesystem relies on the O.S. capabilities, and it could be not entirely reliable.
Testing and contributions in this area are very welcome.


## Serve

Finally, `serve` combines building and watching with an integrated webserver that will listen on the configured
IP and port to serve the generated website.

```bash
$ strange help serve
strange-serve 
Starts a webserver on 127.0.0.1:9876 to serve the generated pages.

USAGE:
    strange serve [OPTIONS] [PATH]

FLAGS:
    -h, --help    Prints help information

OPTIONS:
    -a, --address <ip>    Select the IP the webserver will be listening on. Default: 127.0.0.1
    -p, --port <port>     Select the TCP port the webserver will be listening on. Default: 9876

ARGS:
    <PATH>    Build and serve the project in the specified directory, instead of the current one
```

The syntax is the same as `build` and `watch`, with the addition of options for specifying the IP address and the port
to listen on. If they are not specified on the command line, the values from the [Config](#TODO) will be used.

As for `watch`, the shell console will be locked while the command runs.

*WARNING*: This functionality is meant to be used for testing the website locally only. It is not reliable, secure,
safe or tested enough to be used for a public website.



## Clean

To remove all the generated files from the destination directory, you can use `strange clean`.

This command has no options. It simply deletes everything inside the destination directory in the current project.

There is no confirmation request, and no backup. Use at your own risk.
