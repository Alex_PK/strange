---
title: Template variables
date: 2017-11-15 18:29:18
type: page
---
# Template variables

When a page gets processed by a template engine ([Tera](#TODO) or [Handlebars](#TODO), for now), several variables
are available to control the structure of the output or simply to display information about the page or the website.

The syntax is a little different, depending on the engine. Please see each engine's documentation for the details.

The Tera/Twig/Jinja2 syntax will be used for the examples from now on.

This is a JSON representation of the variables available during a page render:

```json
{
	"title": "Page title",
	"caption": "The caption of the page",
	"author": "Name of the author",
	"date": {
		"year": "2017",
    	"month": "11",
    	"month_short": "Nov",
    	"month_name": "November",
    	"day": "16",
    	"hour": "12",
    	"minute": "34",
    	"second": "56",
		"iso": "2017-11-16T12:34:56Z",
		"full": "2017-11-16 12:34:56",
		"datetime": "2017-11-16 12:34",
		"date": "2017-11-16",
		"date_h": "16 November 2017",
		"date_hs": "16 Nov 2017",
		"time": "12:34",
		"fulltime": "12:34:56",
		"unix": 1234567890,
		"rss": "Thu, 16 Nov 2017 12:34:56 UTC"
	},
	"tags": ["list", "of", "tags"],
	
	"page": {
		// This object contains data set by the plugins for this page
	},
	
	"site": {
		// This object contains data set by the plugins for the whole site
	},
	
	"theme": {
		"name": "Name of the selected theme",
		"path": "The relative path to the theme directory",
		"data": { /* An object containing the data defined in the config file */ }
	},

	"type": "page",
	"url": "URL of the page",

	"name": "File name",
	"dir": "relative directory where the page is",
	"rootpath": "relative path to reach the root of the site",

	"extra": {}
}
```
Most of the fields, especially in the first group, should be self-explanatory.

The date is a special case, as `strange` makes several formats available as pre-computed instead of forcing to
calculate them via the templating language. This can be useful for searching and filtering, as these informations
are available in the list of pages (see below) that can be used to build menus, index pages, etc.

`page` and `site` fields contain information coming from the [plugins](#TODO). They are both namespaced with the
plugin name.

For example, the `nav` plugin makes `page.nav.prev` and `page.nav.next` available to link to the
previous and next page (in date order).
 
And the `tag` plugin makes `site.tag[tagName]` available, with a list of all the pages containing that tag.

`theme` contains meta-information about the theme currently in use, such as the `name` and the `path` (relative to
the current page's path) where the theme generated files are stored. For example, in `/blog/2017/11/16/post.html`,
the `theme.path` var contains `../../../../theme`.

`type` is the type of page as defined in the [Prelude](#TODO), and `url` contains the sull URL of the page, obtained
by appending the website URL as defined in the config file and the current page's path.

`name` and `dir` contain the file name and the path (starting from the destination directory) of the page, while
`rootpath` contains the relative path to the root of the website (alias: the destination directory). For example,
in the previous example of `/blog/2017/11/16/post.html`, `rootpath` contains `../../../../`.

Finally, `extra` contains all the other fileds in the [Prelude](#TODO), not parsed. For example, in this prelude:

```yaml
title: My page
notes: TODO: finish this
```

In the template, a variable named `extra.notes` will contain `TODO: finish this`, as `notes` is not parsed by `strange`.

## Special variable

During the final rendering (when applying the theme's template to the page) an additional variable is available: `body`.

This is a string containing the rendered body of the page. For example:

```html
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>{% block title %}{{ title }}{% endblock title %}</title>
	<link rel="stylesheet" type="text/css" href="{{ theme.path }}css/main.css">
</head>
<body>
<div id="page">
	<header>
		<h1><a href="{{ rootpath }}index.html">{{ site.config.title }}</a></h1>
	</header>

	<main>
		{{ body }}
	</main>
</div>
</body>
</html>
```
