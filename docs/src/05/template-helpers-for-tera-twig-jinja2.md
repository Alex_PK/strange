---
title: Template helpers for Tera/Twig/Jinja2
date: 2017-11-22 09:35:51
type: page
---
# Template helpers for Tera/Twig/Jinja2

There are a few additional filters available when using Tera as a template engine.

## Handling strings, array and objects

### Debug

```twig
<pre>{{ var|debug }}</pre>
```

Dumps the variable contents as a structured string.

Useful for debugging or to know what's inside a template variable.


### JSON encode

```twig
<script>let my_data = {{ var|json_encode }}</pre>

<script>let my_data = {{ var|json_encode(pretty=true) }}</pre>
```

Dumps the variable contents as a JSON representation. 

Useful to pass variables (page metadata, sitemaps, etc.) to Javascript.


### YAML encode

```twig

---
item: {{ var|yaml_encode }}

sub: 
  item: {{ item|yaml_encode(indent=2) }}
```

Dumps the variable contents as a YAML representation. 

Useful to pass variables (page metadata, sitemaps, etc.) to config files.

As there is no way to detect the indentation of the current line, when dumping an indented value, the filter
needs the indentation level.


### Trimming

```twig
{{ var|trim }}

{{ var|ltrim }}

{{ var|rtrim }}

{{ var|trim(c="/.-") }}

```

The `trim` filter removes the specified characters from both ends of the string (or only on the left with `ltrim`, or
the right with `rtrim`).

The `c` parameter is a string containing all the chars to remove, so in the example it will remove any combination
of dashes, dots and slashes.

If no list is given, it will remove space, tab and newline/carriage-return characters.


### Taking part of an array

```twig
{% for k, v in arr|take(n=10, from=4) %}
  ...
{% endfor %}
```

The `take` filter allows to take only the first `n` items of an array, optionally starting from the `from`-th item.

In the example, it will take from the fifth (arrays start from 0) to the fourteenth item.

If not specified, `from` defaults to 0 (the first item).


### Exploding a string into an array

```
{% for i, part in string_value|explode(s="-") %}
  <li>{{ part }}</li>
{% endfor %}
```

The `explode` filter will split a string using the given separator.


### Sorting an array of objects

```twig
{% for k, page in page.nav.pages|sort_objects(k="date.iso", dir="desc") %}
  ...
{% endfor %}
```

This filter allows to sort an array of objects (typically pages, but it can be anything).

The `k` parameter specifies the key, and can be nested with Tera/Twig/Jinja2 syntax.

In this example, it will look for each `page.date.iso` element to sort.

The `dir` parameter specifies the direction, and can be `asc` (the default is not given) for a lower-to-higher
sorting, or `desc`, for higher-to-lower.


### Checking if a string contains a substring

```twig
{% if string_value is starting_with "/" %}
  ...
{% elseif string_value is ending_with ".css" %}
  ...
{% elseif string_value is matching "[Jj](ava)?[Ss](cript)?" %}
  ...
{% endif %}
```


### Checking if arrays and object contain an item

```twig
{% if array_value is containing "a string" %}
  ...
{% elseif object_value is with_key "a string key" %}
  ...
{% endif %}
```

These two testers will check if the given string is in an array or is one of the keys in the object.

*It only works with strings.*


## Filesystem

This set of filters and functions is typically used by themes, as for pages all the files are available in 
template variables. Be careful if you use them in the pages, as they could expose parts of the filesystem
outside the deployed site.

### Getting parts of the path

```twig
- File dir: {{ path|dir_name }}
- File name: {{ path|file_name }}
- File without extension: {{ path|file_stem }}
```

As the name implies, `file_dir` removes the file name (the last component) from the path and returns the parent
directory; `file_name` removes the path, leaving only the file name (the last component); and `file_stem` removes
both the path and the extension (whatever is after the last dot in the file name).


### Composing a path

```twig
{{ path|path_join(name="my-file.css") }}
```

This filter appends the given part (it can be a file name or a directory name) to the path.

It is the recommended way to build paths, instead of joining strings with path separators.


### Reading a file

```twig
{{ path|read_file() }}
```

This filter reads the given file from the filesystem.

In this example it will be printed into the page, but it could be used for other purpouses.

**WARNING**: be careful not to access files outside the deployed website.


### Scanning a directory

```twig
{% for i, filepath in dir_scan(dir=".", filter=".css$") %}
  {{ filepath }}
{% endfor %}
```

This function will create an array with all the files in the given directory, optionally filtering them with
the provided regex.

**WARNING**: be careful not to access files outside the deployed website.
