---
title: Plugins
date: 2017-11-16 09:14:34
type: page
---
# Plugins

Plugins are a collection of additional features that can work at different stages of the building process.

Currently they can be run in 3 phases:

- After the initial app setup

- After scanning the list of pages but before the rendering starts.

- After all documents have been processed.

Plugins can return a list of variables to be added to the `site` object in the templates, or set an entry in the
`page` object of distinct pages.

For example:
 
- the `data` plugin runs before the documents are processed to check if the `data` directory exists

- the `nav` plugin runs before the rendering and sets the `page.nav` object with links to previous and next page

- the `rss` plugin runs after the process is finished, to generate the RSS file.

See the description of each plugin to see which variables they make available to the templates.
