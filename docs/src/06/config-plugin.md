---
title: Config plugin
date: 2017-11-16 09:29:00
type: page
---
# Config plugin

The `config` plugin makes available some configurable options via the `site.config` object.

## Site variables

```json
{
	"config": {
		"title": "Site title",
		"url": "URL of the website",
		"description": "The short site description as set in the config file"
	}
}
``` 

Example:

```html
<html>
<head><title>{{ site.config.title }}</title></head>
<body>
  <header>
    <h1>{{ site.config.title }}</h1>
    <span class="motto">{{ site.config.description }}</span>
</header>
</body>
</html>
```
