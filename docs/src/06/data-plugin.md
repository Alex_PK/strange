---
title: Data plugin
date: 2017-11-16 18:15:15
type: page
---
# Data plugin

This plugin scans a directory named `data` in the root of the project (**not** inside `src`), 
looking for YAML files and transforming them into a tree structure.

The retrieved information are then made available via the `site.data` object in the templates.

Nested subdirectories are not supported. 

Files can have `.yml` or `.yaml` extension and must be well-formed (with the initial `---` on its own line).

YAML multi-documents are not supported: only the first one will be parsed.

## Site variables

All the retrieved data will be available in the `site` object.

### Example

Given a structure like this:

```
+ src/
|  + page1.md
|  ...
| data/
|  + info.yml
|  + server.yaml
| build/
```

If info.yml is

```

---
name: My name
address:
  street: somewhere
  number: 123
  exists: false
  tags:
    - one
    - two
    - three
```

and `server.yaml` is

```

---
url: example.net
port: 8080
```

The following can be used in a template:

```html
<h2>Server</h2>
<span class="hostname">{{ site.data.server.url }}:{{ site.data.server.port }}</span>

<h2>Me</h2>
HI, I'm {{ site.data.info.name }} and I live {{ site.data.info.address.street }}.
```


## Configuration

The directory containing the data files can be configured in the `strange.yaml` config file:

```yaml
plugins:
  data:
    dir: mydatadir
```

**NOTE**: It is recommended to not use the source, the destination or the template directories to
store data files (or the `data` plugin directory), as this could lead to loss of data or
spurious files generation during the build.
