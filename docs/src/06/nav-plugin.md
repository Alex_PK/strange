---
title: Nav plugin
date: 2017-11-16 18:15:21
type: page
---
# Nav plugin

The `nav` plugin creates a lot of navigation info, both in the `site` and in the `page` objects.

## Page variables

On every page, it makes available an object `site.nav` containing:

```json
{
	"nav": {
		"prev": {
			"title": "Page title",
			"caption": "The caption of the page",
			"author": "Name of the author",
			"date": {
				"year": "2017",
				"month": "11",
				"day": "16",
				"hour": "12",
				"minute": "34",
				"second": "56",
				"iso": "2017-11-16T12:34:56Z",
				"full": "2017-11-16 12:34:56",
				"datetime": "2017-11-16 12:34",
				"date": "2017-11-16",
				"time": "12:34",
				"fulltime": "12:34:56"
			},
			"tags": ["list", "of", "tags"],
			"type": "page",
			"url": "URL of the page",
		
			"name": "File name",
			"dir": "relative directory where the page is",
			"rootpath": "relative path to reach the root of the site"
		},
		"next": { /* same structure as prev*/ },
		"pages": [
			{ /* same structure as prev */ }
		],
		"dirs": {
			"dirname": { /* same structure as prev */ }
		},
		"files": [
			{ /* same structure as prev */ }
		]
	}
}
```

`prev` and `next` contain complete meta-data (as described in the [Template variables](#TODO) chapter)
about the previous and following page, sorted by file name.

The first page will have `null` as `prev`, and the last one will have `null` as `next`.

`pages` and `files` are 2 arrays of meta-data objects containing all the pages (HTML documents)
and files (everything except the HTML documents) in the same directory.

`dir` is an object where keys are the directory names, and the content of each are meta-data of the (real or
generated) index file in that dir.

These information can be used to create navigation links in a blog or indexes:

```html
<nav>
	{% if page.nav.prev %}
		<a class="prev" href="{{ page.nav.prev.url }}">{{ page.nav.prev.title }}</a>
	{% endif %}
	{% if page.nav.next %}
		<a class="next" href="{{ page.nav.next.url }}">{{ page.nav.next.title }}</a>
	{% endif %}
</nav>
```

## Site variables

This plugin also provides a flattened sitemap of the whole site in the `site` object:

```json
{
	"nav": [
		{
			"title": "Page title",
			"caption": "The caption of the page",
			"author": "Name of the author",
			"date": {
				"year": "2017",
				"month": "11",
				"day": "16",
				"hour": "12",
				"minute": "34",
				"second": "56",
				"iso": "2017-11-16T12:34:56Z",
				"full": "2017-11-16 12:34:56",
				"datetime": "2017-11-16 12:34",
				"date": "2017-11-16",
				"time": "12:34",
				"fulltime": "12:34:56"
			},
			"tags": ["list", "of", "tags"],
			"type": "page",
			"url": "URL of the page",
		
			"name": "File name",
			"dir": "relative directory where the page is",
			"rootpath": "relative path to reach the root of the site"
		},
		{
			/* every page meta-data */
		}
	]
}
```

This object is useful to have a global view of the whole website and be able to filter, for example in the themes
or when building indx pages.
