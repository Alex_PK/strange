---
title: RSS plugin
date: 2017-11-16 18:15:29
type: page
---
# RSS plugin

This plugin generates an RSS file with the latest articles. By default it will generate
a file named `rss.xml` in the configured destination directory (`build/` by default)
containing the latest 10 articles, as detected by their `date` metadata field 
(see [Create a new page](#create-a-new-page) for more information).


## Site variables

This plugin also exposes a variable you can use in your templates to have the URL to the
generated file. You must configure the site URL in the `strange.yaml` config file.

### Example

```html
<!doctype html>
<html>
<head>
  <link 
    href="{{ site.rss.url }}" 
    rel="alternate" 
    type="application/rss+xml" 
    title="My wonderful web site" 
  />
</head>
<body>
  {{ body }}
</body>
</html>
```


## Configuration

The plugin can be configured in the `strange.yaml` config file:

```yaml
plugins:
  rss:
    enabled: true
    filename: rss.xml
    num: 10
```

The available options are:

- `enabled`: if set to `true`, it will generate the file. If set to `false`, it will not.
  Default: `true`

- `filename`: the path to the filename, relative to the destination directory.
  For example: `feeds/rss.xml` or `feed.xml` or `some/deep/dir/myfeed.rss.xml`
  Default: `rss.xml`

- `num`: the number of pages to include in the feed.
  Default: `10`
