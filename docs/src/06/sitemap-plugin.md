---
title: Sitemap plugin
date: 2017-11-16 18:15:36
type: page
---
# Sitemap plugin

This plugin provides a tree of all the pages in the site.

## Page variables

`page.sitemap` contains:

- `pages` (array)
	This array contains a list of all the pages (HTML documents) at a certain level of
	the tree as Page objects, sorted by file name. 
	
- `files` (array)
	This array contains a list of all the files that are not pages, in the current directory.
	
- `dirs` (object)
	This is an object in which every key is the name of the containing
	directory, and each item is an object containing the same keys as the
	"root" object (pages and children).

The structure is recursive. This is a representation of it in JSON (every file will contain full meta-data. They were
left out in this example to save space)

```json
{
	"sitemap": {
		"pages": [
			{
				"title": "Site index",
				"name": "index.html"
			}
		],
		"files": [
			{
				"name": "favicon.ico"
			}
		],
		"dirs": {
			"js": {
				"pages": [],
				"files": [
					{
						"name": "main.js"
					}
				],
				"dirs": {}
			},
			"css": {
				"pages": [],
				"files": [
					{
						"name": "main.css"
					}
				],
				"dirs": {}
			}
		}
	}
}
```


### Example

```html
<h2>Sitemap pages</h2>
<ul>
	{% for (dir, data) in  site.sitemap.folders %}
	    <li class="dir">{{ dir }}
	        <ul>
				{% for page in data.pages %}
			        <li class="page">
			            <a href="{{ page.url }}">
			                {{ page.title }} <time>{{ page.date.full }}</time>
			            </a>
			        </li>
				{% endfor %}
	        </ul>
	     </li>
	{% endfor %}

	{% for page in  site.sitemap.pages %}
        <li class="page">
            <a href="{{ page.url }}">
                {{ page.title }} <time>{{ page.date.full }}</time>
            </a>
        </li>
	{% endfor %}
</ul>
```
