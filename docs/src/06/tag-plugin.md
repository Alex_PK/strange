---
title: Tag plugin
date: 2017-11-16 18:15:42
type: page
---
# Tag plugin

This plugin groups all the pages in the site by tag

## Site variables

`site.tag` is an object where keys are tags and each contains an array of page's meta-data objects.

```json
{
	"tag": {
		"tag-name": [
			{
				"title": "..."
			},
			{
				"title": "..."
			}
		],
		"other-tag": [
			{
				"title": "..."
			}
		]
	}
}
```

In this representation most of meta-data has been left out to save space, but it will be available in the templates.

As every page can have multiple tags, the page will appear under each of the associated tags.


### Example

In Tera:

```html
<h2>Tags</h2>
<ul>
	{% for (name, pages) in  site.tag %}
		<li>{{ name }}
			<ul>
				{% for page in pages %}
					<li>
						<a href="{{ page.url }}">
							{{ page.title }}
						</a>
					</li>
				{% endfor %}
            </ul>
		</li>
	{% endfor %}
</ul>
```
