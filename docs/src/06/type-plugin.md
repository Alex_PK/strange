---
title: Type plugin
date: 2017-11-16 18:15:50
type: page
---
# Type plugin

### Type

This plugin groups all the pages in the site by type, as specified in the page meta-data section.

## Site variables

`site.type` is an object where keys are types and each contains an array of Page objects.

```json
{
	"type": {
		"page": [
			{
				"title": "..."
			},
			{
				"title": "..."
			}
		],
		"post": [
			{
				"title": "..."
			}
		]
	}
}
```

In this representation most of meta-data has been left out to save space, but it will be available in the templates.

As a page can only have one type, it will appear only once, under the corresponding type.

#### Example

In Tera:

```html
<h2>Tags</h2>
<ul>
	{% for (name, pages) in  site.type %}
		<li>{{ name }}
			<ul>
				{% for page in pages %}
					<li>
						<a href="{{ page.url }}">
							{{ page.title }}
						</a>
					</li>
				{% endfor %}
            </ul>
		</li>
	{% endfor %}
</ul>
```
