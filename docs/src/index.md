---
title: Strange intro
date: 2017-11-10 18:18:01
type: page
---
# What is Strange?

Strange is a static site generator.

It allows you to model your website freely, while helping by converting commonmark (standardized markdown) files
into HTML pages, applying themes, providing helpers for the templates, and other little things.


## Download

You can find [strange on crates.io](https://crates.io/crates/strange).

If you have a recent version of Rust installed (>=1.18) you can install it with:

```bash
cargo install strange
```

The binary will be created in `~/.cargo/bin/strange`.

Or you can download a pre-compiled binary for Linux/amd64 from the `Downloads` section on [Strange's bitbucket](#TODO).

