use std::env;
use std::path::{Path, PathBuf};

use crate::config::Config;
use crate::theme::Themes;
use crate::plugin::Plugins;

use crate::errors::*;

pub struct App {
	config: Config,
	themes: Themes,
	plugins: Plugins,
}

impl App {
	pub fn new<P>(path: P) -> Result<App>
		where P: AsRef<Path>
	{
		let mut path = path.as_ref().to_owned();
		let path: Result<PathBuf> = if PathBuf::from("") != path {
			if !path.is_absolute() {
				path = env::current_dir().or_err(|| "Cannot retrieve current dir")?.join(path);
			}
			if !path.exists() {
				Err(format!("Path `{}` does not exist", path.display()).into())
			} else {
				Ok(path)
			}
		} else {
			env::current_dir().or_err(|| "Cannot retrieve current dir")
		};

		App::from(&path?)
	}

	fn from<P>(path: P) -> Result<App>
		where P: AsRef<Path>
	{
		let plugins = Plugins::new()?;
		let plugins_defaults = plugins.config_defaults();
		let config = Config::new(path.as_ref())
			.unwrap_or_else(|e| {
				error!("Config file is missing or corrupt: {}", e);
				let mut config = Config::default();
				config.plugins.merge(plugins_defaults);
				config
			});
		let themes = Themes::new(&config)?;

		Ok(App {
			config,
			themes,
			plugins,
		})
	}

	pub fn config_mut(&mut self) -> &mut Config {
		&mut self.config
	}

	pub fn config(&self) -> &Config {
		&self.config
	}

	pub fn plugins(&self) -> &Plugins {
		&self.plugins
	}

	pub fn themes(&self) -> &Themes {
		&self.themes
	}
}
