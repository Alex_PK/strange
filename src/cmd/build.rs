/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use rayon;
use rayon::prelude::*;
use std::fs;
use std::path::{Path, PathBuf};
use time;

use crate::app::App;
use crate::cmd;
use crate::config::Config;
use crate::errors::*;
use crate::models::{Directory, File};
use crate::tpl;
use crate::utils;
use crate::utils::print;

pub fn run<P>(path: P) -> Result<String>
where
	P: AsRef<Path>,
{
	let build_ts_start = time::get_time();
	let app = App::new(path)?;

	let config = app.config();

	info!("Building...");

	let (source_path, dest_path) = prepare_paths(&app)?;
	trace!(" Src: {:?}", &source_path);
	trace!("Dest: {:?}", &dest_path);

	let engines = tpl::Engines::new(config)?;
	let index = Directory::scan_from(&source_path, config)?;

	debug!("Parallelizing on {} threads", rayon::current_num_threads());

	let mut files = Vec::new();
	generate_paths(config, &index, &dest_path, &mut files)?;
	info!("Found {} files", files.len());

	info!("Parsing meta-data");
	let ts_start = time::get_time();
	let _: Vec<Result<()>> = files
		.par_iter()
		.map(|f| {
			f.parse_meta(config, &engines).map_err(|e| {
				dump_error(&e);
				e
			})
		})
		.collect();
	let ts_end = time::get_time();
	print::dprofiling("Parsing", &ts_start, &ts_end);

	info!("Processing plugins");
	let ts_start = time::get_time();
	let plugin_results = app.plugins().before_render(config, &index)?.get_data();
	let ts_end = time::get_time();
	print::dprofiling("Processing", &ts_start, &ts_end);

	info!("Rendering");
	let ts_start = time::get_time();
	let _: Vec<Result<()>> = files
		.par_iter()
		.map(|f| {
			f.render(config, &engines, &dest_path, &plugin_results).map_err(|e| {
				dump_error(&e);
				e
			})
		})
		.collect();
	let ts_end = time::get_time();
	print::dprofiling("Rendering", &ts_start, &ts_end);

	info!("Post-processing plugins");
	let ts_start = time::get_time();
	let _ = app.plugins().after_render(config, &index)?.get_data();
	let ts_end = time::get_time();
	print::dprofiling("Post-processing", &ts_start, &ts_end);

	let themes = app.themes();

	info!("Processing theme");
	let ts_start = time::get_time();
	match themes.get_current() {
		Some(theme) => {
			theme.render(&plugin_results)?;
		}
		None => warn!("No theme selected in the config file"),
	}
	let ts_end = time::get_time();
	print::dprofiling("Processing", &ts_start, &ts_end);

	Ok(format!(
		"Build finished in {}s.",
		utils::time_diff_in_ms(&build_ts_start, &ts_end) as f64 / 1000.0
	))
}

fn prepare_paths(app: &App) -> Result<(PathBuf, PathBuf)> {
	cmd::clean::clean_dest(&app.config().paths.dest())?;
	let config = app.config();

	let source_path = config.paths.src();
	let mut dest_path = config.paths.dest();

	if dest_path.starts_with("./") {
		dest_path = dest_path
			.strip_prefix("./")
			.map(|v| v.to_owned())
			.or_err(|| format!("Unable to strip ./ from path to file '{}'", dest_path.display()))?;
	}

	Ok((source_path, dest_path))
}

fn generate_paths<'index, P>(
	config: &Config,
	dir: &'index Directory,
	dest_path: P,
	files: &mut Vec<&'index File>,
) -> Result<()>
where
	P: AsRef<Path>,
{
	let dest_path = dest_path.as_ref();
	let dest = dest_path.join(&dir.path);
	trace!("creating dir {:?}", dest);

	fs::create_dir_all(&dest).or_err(|| format!("Unable to create path  '{:?}'", dest).to_string())?;

	for dir in &dir.dirs {
		generate_paths(config, dir, dest_path, files)?;
	}

	for file in &dir.files {
		files.push(file);
	}

	Ok(())
}
