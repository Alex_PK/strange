/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs;
use std::path::Path;

use crate::app::App;
use crate::errors::*;

pub fn run() -> Result<String> {
	let app = App::new("")?;
	let config = app.config();
	clean_dest(&config.paths.dest())?;

	Ok("Build cleaned".into())
}

pub fn clean_dest<P>(path: P) -> Result<()>
where
	P: AsRef<Path>,
{
	let path = path.as_ref();
	if path.exists() {
		fs::remove_dir_all(&path).or_err(|| format!("Cannot clean build directory `{}`", path.display()))?;
	}

	fs::create_dir_all(&path).or_err(|| format!("Unable to create build directory `{}`", path.display()))
}
