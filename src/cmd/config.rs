/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use crate::app::App;
use crate::errors::*;

pub fn update() -> Result<String> {
	let mut app = App::new("")?;

	let plugins_defaults = app.plugins().config_defaults();
	let config = app.config_mut();
	config.plugins.merge(plugins_defaults);

	debug!("Config: {:?}", config);

	config.write()?;
	Ok("Config updated".into())
}
