/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::env;
use std::fs;
use std::path::Path;

use crate::config::Config;
use crate::errors::*;
use crate::plugin::Plugins;

pub fn run<P>(project_dir: Option<P>, source: Option<&str>, dest: Option<&str>) -> Result<String>
where
	P: AsRef<Path>,
{
	// TODO: better check if the project is already created
	if let Some(dir) = project_dir {
		let dir = dir.as_ref();
		fs::create_dir(dir).or_err(|| format!("Unable to create requested project '{}'", dir.display()))?;
		env::set_current_dir(dir).or_err(|| "Cannot set current directory")?;
	};

	let base_dir = env::current_dir().or_err(|| "Cannot set current directory")?;

	let mut config = Config::default();
	if let Some(v) = source {
		config.paths.src = v.to_string()
	};
	if let Some(v) = dest {
		config.paths.dest = v.to_string()
	};

	let plugins = Plugins::new()?;
	let plugins_defaults = plugins.config_defaults();
	config.plugins.merge(plugins_defaults);
	config.write()?;

	/*
	 * Create directories
	 */

	for dir_to_create in [&config.paths.src, &config.paths.dest, &config.paths.themes].iter() {
		let full_path = base_dir.join(dir_to_create);
		if Path::new(&full_path).exists() {
			continue;
		}
		fs::create_dir_all(&full_path)
			.or_err(|| format!("Unable to create requested project directory '{}'", full_path.display()))?;
	}

	plugins.post_init(&config, &base_dir)?;

	Ok("Project initialized".into())
}
