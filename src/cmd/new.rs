/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use slug;
use std::fs;
use std::path::Path;
use time;

use crate::app::App;
use crate::errors::*;
use crate::tpl;

pub fn run<P>(title: &str, path: P, date: bool, time: bool, prefix: Option<&str>, typ: Option<&str>) -> Result<String>
where
	P: AsRef<Path>,
{
	let mut app = App::new("").or_err(|| "Cannot initialize app")?;
	let path = path.as_ref();
	let typ = typ.unwrap_or_else(|| "page");

	{
		let config = app.config_mut();
		if date {
			config.pages.date = true;
		};
		if time {
			config.pages.time = true;
		};
	}

	let config = app.config();

	if title == "" {
		error!("No article title given");
		return Err("No title provided".into());
	}

	let doc_dt = time::strftime("%F %H:%M:%S", &(time::now())).or_err(|| "Unable to format the date")?;

	let mut dest_path = config.paths.src().join(path);
	fs::create_dir_all(&dest_path)
		.or_err(|| format!("Unable to change working directory to {}", dest_path.display()))?;

	let mut parts = Vec::new();

	if config.pages.date || config.pages.time {
		let _ = time::strftime("%F", &time::now()).map(|v| parts.push(v));
		if config.pages.time {
			let _ = time::strftime("%H%M", &time::now()).map(|v| parts.push(v));
		}
	}

	if let Some(p) = prefix {
		parts.push(p.to_string())
	};
	parts.push(slug::slugify(title));

	debug!("Parts: {:?}", parts);

	let mut filename = parts.join("-");
	filename.push_str(".md");
	debug!("Generated filename `{}`", &filename);

	dest_path.push(&filename);

	if dest_path.exists() {
		return Err(format!("File `{}` already exists", dest_path.display()).into());
	}

	let engines = tpl::Engines::new(config)?;
	// We force markdown-only pages for now. Maybe in the future we'll add a flag
	//	engines.write_page(&tpl::EngineType::from(&config.templates.engine), dest_path, &title, &doc_dt, typ);
	engines.write_page(&tpl::EngineType::Markdown, dest_path, title, &doc_dt, typ)?;

	Ok(format!(
		"Page created: `{}`.",
		vec![path.to_str().unwrap_or(""), &filename].join("/")
	))
}
