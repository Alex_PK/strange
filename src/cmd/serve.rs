/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::path::Path;
use std::thread;
use simple_server::{Server, Method, StatusCode};

use crate::app::App;
use crate::cmd::watch;
use crate::errors::*;

pub fn run<P>(ip: Option<&str>, port: Option<&str>, path: P) -> Result<String>
where
	P: AsRef<Path>,
{
	let mut app = App::new(&path)?;
	{
		let config = app.config_mut();
		if let Some(v) = ip { config.serve.ip = v.to_string() };
		if let Some(v) = port { config.serve.port = v.to_string() };
	}

	let config = app.config();
	let ip = config.serve.ip.clone();
	let port = config.serve.port.clone();

	let monitor_path = config.paths.dest();

	let serve_path = config.paths.root().join(&config.paths.dest);
	let serve_handle = thread::spawn(move || {
		let mut server = Server::new(|request, mut response| {
			info!("Request received. {} {}", request.method(), request.uri());
			match (request.method(), request.uri().path()) {
				(&Method::GET, "/") => {
					response.status(StatusCode::FOUND);
					response.header("Location","/index.html");
					Ok(response.body("".as_bytes().to_vec())?)
				}
				(_, _) => {
					response.status(StatusCode::NOT_FOUND);
					Ok(response.body(b"Page not found or rendering not completed yet. Please retry.".to_vec())?)
				}
			}

		});
		server.set_static_directory(serve_path);

		info!("Listening on http://{}:{}", &ip, &port);

		server.listen(&ip, &port);
	});

	let _ = watch::run(&monitor_path);
	let _ = serve_handle.join();

	Ok("Serve ended".into())
}
