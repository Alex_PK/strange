/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use crate::app::App;
use crate::errors::*;

pub fn info() -> Result<String> {
	let app = App::new("")?;
	let themes = app.themes();
	match themes.get_current() {
		Some(theme) => {
			info!("Current theme: {}", theme.name);
			info!("Generating into: {}", themes.get_dest().display());
			info!("Name: {}", theme.meta.name);
			info!("Version: {}", theme.meta.version);
			info!("Author: {}", theme.meta.author);
		}
		None => warn!("No current theme defined"),
	}

	Ok("OK".into())
}
