/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};
use std::path::{Component, Path};
use std::sync::mpsc::channel;
use std::time::Duration;

use crate::app::App;
use crate::cmd::build;
use crate::config::Config;
use crate::errors::*;
use crate::utils::files;

pub fn run<P>(path: P) -> Result<String>
where
	P: AsRef<Path>,
{
	let app = App::new(&path)?;
	let config = app.config();

	let (tx, rx) = channel();

	// Always build at least once before starting to watch
	match build::run(config.paths.root()) {
		Ok(msg) => info!(msg),
		Err(e) => {
			dump_error(&e);
		}
	}

	let mut watcher = watcher(tx, Duration::from_millis(200)).or_err(|| "Cannot initialize watcher")?;
	watcher
		.watch(&config.paths.root(), RecursiveMode::Recursive)
		.or_err(|| "Cannot start watcher")?;

	info!("Watching for file changes in {}", config.paths.root().display());

	loop {
		let mut do_render = false;
		while let Ok(event) = rx.recv_timeout(Duration::from_millis(500)) {
			match event {
				DebouncedEvent::Create(path) | DebouncedEvent::Write(path) => {
					if is_path_to_process(&path, config) {
						debug!("Triggering build on {} changes", path.display());
						do_render = true;
					}
				}
				_ => {}
			}
		}
		if do_render {
			match build::run(config.paths.root()) {
				Ok(msg) => info!(msg),
				Err(e) => {
					dump_error(&e);
				}
			}
		}
	}
}

fn is_path_to_process(path: &Path, config: &Config) -> bool {
	if path.is_dir() {
		return false;
	}

	let file_name: &str = path
		.file_name()
		.and_then(|name| name.to_str())
		.unwrap_or_else(|| "");

	let dir = match path.strip_prefix(&config.paths.root()).unwrap().components().nth(0) {
		Some(Component::Normal(name)) => name.to_str().unwrap_or_else(|| ""),
		_ => "",
	}
	.to_owned();

	!(dir.is_empty()
		|| files::is_filename_not_allowed(&dir)
		|| dir == config.paths.dest
		|| (dir == config.paths.src && files::is_filename_not_allowed(file_name)))
}
