/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::{io, fs, env};
use std::path::{Path, PathBuf};
use std::collections::BTreeMap;
use std::sync::{Arc, RwLock};
use chrono::Utc;
use serde_yaml;
use serde_json::Value;

use crate::errors::*;
use crate::utils::files;
use crate::plugin::Object;
use crate::tpl::engine_tera;

impl Config {
	pub fn new<P>(p: P) -> Result<Config>
		where P: AsRef<Path>
	{
		let p = p.as_ref();
		let config_path = p.join("strange.yaml");
		match fs::File::open(&config_path) {
			Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
				match p.parent() {
					Some(p) if p.exists() => {
						Config::new(p)
					}
					_ => Err("Config file not found".into())
				}
			}
			Ok(f) => {
				serde_yaml::from_reader(&f)
					.map(|config: Config| {
						debug!(format!("Found config: {:?} in {:?}", config_path.display(), p));
						{
							let mut root_path = config.paths.root.write().unwrap();
							*root_path = p.to_owned();
						}
						config
					})
					.or_err(|| "Error while decoding config file")
			}
			Err(e) => Err(Error::new("Error reading the config file").with_cause(e))
		}
	}

	pub fn write(&self) -> Result<()> {
		let tera = engine_tera::Tera::new("")?;
		let dest_path = self.paths.root().join("strange.yaml");
		let file_time = files::file_time(&dest_path).unwrap_or_else(Utc::now);

		let new_config = tera.render(include_str!("tpl/files/config.yaml.twig"), self)?;
		let _ = files::read_to_string(&dest_path)
			.and_then(|v| {
				if new_config != v {
					let bak_name = file_time.format("strange.yaml.%F-%H%M%S.bak").to_string();
					let bak_path = self.paths.root().join(&bak_name);
					let _ = fs::rename(&dest_path, &bak_path);
				}
				Ok(())
			});
		//	let new_config = serde_yaml::to_string(config).unwrap();
		files::write_to_file(&dest_path, &new_config)
	}

}

#[derive(Serialize, Deserialize, Default, Debug, Clone)]
pub struct Config {
	#[serde(default)]
	pub site: Site,

	#[serde(default)]
	pub pages: Pages,

	#[serde(default)]
	pub paths: Paths,

	#[serde(default)]
	pub serve: Serve,

	#[serde(default)]
	pub plugins: Plugins,

	#[serde(default)]
	pub theme: Theme,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Site {
	#[serde(default = "dfl_site_title")]
	pub title: String,

	#[serde(default = "dfl_site_url")]
	pub url: String,

	#[serde(default)]
	pub description: String,

	#[serde(default = "dfl_site_indexes")]
	pub indexes: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Pages {
	#[serde(default)]
	pub date: bool,

	#[serde(default)]
	pub time: bool,

	#[serde(default = "dfl_pages_author")]
	pub author: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Paths {
	#[serde(default = "dfl_paths_root", skip)]
	pub root: Arc<RwLock<PathBuf>>,

	#[serde(default = "dfl_paths_src")]
	pub src: String,

	#[serde(default = "dfl_paths_dest")]
	pub dest: String,

	#[serde(default = "dfl_paths_themes")]
	pub themes: String
}

impl Paths {
	pub fn root(&self) -> PathBuf {
		self.root.read().unwrap().to_owned()
	}

	pub fn set_root<P>(&self, root: P) where P: AsRef<Path> {
		let root = root.as_ref();
		*(self.root.write().unwrap()) = root.into();
	}

	pub fn src(&self) -> PathBuf {
		self.root().join(&self.src).to_owned()
	}

	pub fn dest(&self) -> PathBuf {
		self.root().join(&self.dest).to_owned()
	}

	pub fn themes(&self) -> PathBuf {
		self.root().join(&self.themes).to_owned()
	}
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Serve {
	#[serde(default = "dfl_serve_ip")]
	pub ip: String,

	#[serde(default = "dfl_serve_port")]
	pub port: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Theme {
	#[serde(default = "dfl_theme_name")]
	pub name: String,

	#[serde(default = "dfl_theme_dest")]
	pub dest: String,

	#[serde(default = "dfl_theme_options")]
	pub options: Object,

	#[serde(default = "dfl_theme_data")]
	pub data: Object,
}

#[derive(Serialize, Deserialize, Default, Debug, Clone)]
pub struct Plugins(BTreeMap<String, Object>);

//impl Default for Plugins {
//	fn default() -> Self {
//		Plugins(PLUGINS.config_defaults())
//	}
//}

impl Plugins {
	pub fn merge(&mut self, defaults: BTreeMap<String, Object>) {
		let mut defaults = defaults.clone();
		for (id, data) in &self.0 {
			let entry = defaults.entry(id.to_string()).or_insert_with(Object::new);
			for (key, value) in data {
				entry.insert(key.clone(), value.clone());
			}
		}

		self.0 = defaults;
	}

	//	pub fn set<V>(&mut self, plugin: &str, key: &str, val: V) -> &mut Self
	//		where Value: From<V>
	//	{
	//		let val = Value::from(val);
	//		*self.0
	//			.entry(plugin.into())
	//			.or_insert_with(|| Object::new())
	//			.entry(key)
	//			.or_insert(Value::Null)
	//			= val;
	//
	//		self
	//	}

	pub fn get(&self, plugin: &str, key: &str) -> Option<Value> {
		self.0
			.get(plugin)
			.and_then(|config| {
				config
					.get(key)
					.cloned()
			})
	}

	pub fn get_string(&self, plugin: &str, key: &str) -> Option<String> {
		self.get(plugin, key)
			.and_then(|s| {
				s.as_str()
					.map(|s| s.to_string())
			})
	}

	pub fn get_int(&self, plugin: &str, key: &str) -> Option<i64> {
		self.get(plugin, key)
			.and_then(|s| {
				s.as_i64()
			})
	}

	pub fn get_bool(&self, plugin: &str, key: &str) -> Option<bool> {
		self.get(plugin, key)
			.and_then(|s| {
				s.as_bool()
			})
	}

	//	pub fn get_object(&self, plugin: &str, key: &str) -> Option<Object> {
	//		self.get(plugin, key)
	//			.and_then(|s| {
	//				s.as_object()
	//					.map(|s| s.to_owned())
	//			})
	//	}
}

impl Default for Site {
	fn default() -> Self {
		Site {
			title: dfl_site_title(),
			url: dfl_site_url(),
			description: String::new(),
			indexes: dfl_site_indexes(),
		}
	}
}

impl Default for Pages {
	fn default() -> Self {
		Pages {
			time: false,
			date: false,
			author: dfl_pages_author(),
		}
	}
}

impl Default for Paths {
	fn default() -> Self {
		Paths {
			root: dfl_paths_root(),
			src: dfl_paths_src(),
			dest: dfl_paths_dest(),
			themes: dfl_paths_themes(),
		}
	}
}

impl Default for Serve {
	fn default() -> Self {
		Serve {
			ip: dfl_serve_ip(),
			port: dfl_serve_port(),
		}
	}
}

impl Default for Theme {
	fn default() -> Self {
		Theme {
			name: dfl_theme_name(),
			dest: dfl_theme_dest(),
			options: dfl_theme_options(),
			data: dfl_theme_data(),
		}
	}
}

pub fn dfl_site_title() -> String { "My strange site".into() }

pub fn dfl_site_url() -> String { "http://example.net".into() }

pub fn dfl_pages_author() -> String { "Unknown".into() }

pub fn dfl_site_indexes() -> bool { true }

pub fn dfl_paths_root() -> Arc<RwLock<PathBuf>> { Arc::new(RwLock::new(env::current_dir().unwrap())) }

pub fn dfl_paths_src() -> String { "src".into() }

pub fn dfl_paths_dest() -> String { "build".into() }

pub fn dfl_paths_themes() -> String { "themes".into() }

pub fn dfl_serve_ip() -> String { "127.0.0.1".into() }

pub fn dfl_serve_port() -> String { "9876".into() }

pub fn dfl_theme_name() -> String { "default".into() }

pub fn dfl_theme_dest() -> String { "theme".into() }

pub fn dfl_theme_options() -> Object { Object::new() }

pub fn dfl_theme_data() -> Object { Object::new() }
