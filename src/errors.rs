/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use backtrace::Backtrace;
use std::error::Error as StdError;
use std::fmt::{Display, Formatter};
use std::sync::{Arc};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum ErrorKind {
	General,
	ThemeEngine,
	Theme(String),
	TemplateEngine(String),
	Template(String, String),
}

impl Display for ErrorKind {
	fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
		match self {
			ErrorKind::General => Ok(()),
			ErrorKind::ThemeEngine => Ok(()),
			ErrorKind::Theme(s) => write!(f, " (Theme: {})", s),
			ErrorKind::TemplateEngine(engine) => write!(f, " (Templates: {})", engine),
			ErrorKind::Template(engine, tpl) => write!(f, " (Template({}): {})", engine, tpl),
		}
	}
}

#[derive(Debug)]
pub struct Error {
	msg: String,
	kind: ErrorKind,
	cause: Option<Arc<StdError>>,
	backtrace: Backtrace,
}

unsafe impl Send for Error {}
unsafe impl Sync for Error {}

impl Error {
	pub fn new<S: Into<String>>(msg: S) -> Error {
		Error {
			msg: msg.into(),
			kind: ErrorKind::General,
			cause: None,
			backtrace: Backtrace::new(),
		}
	}

	pub fn with_kind(self, kind: ErrorKind) -> Error {
		Error {
			kind,
			..self
		}
	}

	pub fn with_cause<E: StdError + 'static>(self, cause: E) -> Error {
		Error {
			cause: Some(Arc::new(cause)),
			..self
		}
	}

	pub fn backtrace(&self) -> Option<&Backtrace> {
		Some(&self.backtrace)
	}

	pub fn iter(&self) -> Causes {
		Causes {
			e: match &self.cause {
				Some(c) => Some(c.as_ref()),
				None => None,
			},
		}
	}
}

impl StdError for Error {
	fn description(&self) -> &str {
		&self.msg
	}

	fn cause(&self) -> Option<&StdError> {
		match &self.cause {
			Some(c) => Some(c.as_ref()),
			None => None,
		}
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
		write!(f, "{}{}", &self.msg, self.kind)
	}
}

impl From<&str> for Error {
	fn from(s: &str) -> Self {
		Error::new(s.to_string())
	}
}

impl From<String> for Error {
	fn from(s: String) -> Self {
		Error::new(s)
	}
}

impl From<(String, ErrorKind)> for Error {
	fn from(v: (String, ErrorKind)) -> Self {
		Error::new(v.0).with_kind(v.1)
	}
}

impl From<(&str, ErrorKind)> for Error {
	fn from(v: (&str, ErrorKind)) -> Self {
		Error::new(v.0.to_string()).with_kind(v.1)
	}
}

pub trait ResultExt<T> {
	fn or_err<K: Into<Error>, O: FnOnce() -> K>(self, op: O) -> std::result::Result<T, Error>;
}

impl<T, E: std::error::Error + 'static> ResultExt<T> for std::result::Result<T, E> {
	fn or_err<K: Into<Error>, O: FnOnce() -> K>(self, op: O) -> std::result::Result<T, Error> {
		match self {
			Ok(t) => Ok(t),
			Err(e) => Err(op().into().with_cause(e)),
		}
	}
}

pub trait OptionExt<T> {
	fn or_err<K: Into<Error>, O: FnOnce() -> K>(self, op: O) -> std::result::Result<T, Error>;
}

impl<T> OptionExt<T> for std::option::Option<T> {
	fn or_err<K: Into<Error>, O: FnOnce() -> K>(self, op: O) -> std::result::Result<T, Error> {
		match self {
			Some(t) => Ok(t),
			None => Err(op().into()),
		}
	}
}

pub struct Causes<'e> {
	e: Option<&'e StdError>,
}

impl<'e> Iterator for Causes<'e> {
	type Item = &'e StdError;
	fn next(&mut self) -> Option<&'e StdError> {
		self.e.map(|err| {
			self.e = err.cause();
			err
		})
	}
}

pub fn dump_error(e: &Error) {
	error!("{}", e);

	for e in e.iter().skip(1) {
		error!("caused by: {}", e);
	}

	if let Some(backtrace) = e.backtrace() {
		error!("backtrace: {:?}", backtrace);
	}
}
