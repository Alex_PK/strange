/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::path::{Path, PathBuf};
use std::{env, fs};

use crate::config::Config;
use crate::errors::*;
use crate::models::File;
use crate::utils::files;

#[derive(Debug)]
pub struct Directory {
	pub path: PathBuf,
	pub fullpath: PathBuf,
	pub name: String,
	pub url: Option<String>,
	pub dirs: Vec<Directory>,
	pub files: Vec<File>,
}

impl Directory {
	pub fn new<P1, P2>(p: P1, basepath: P2) -> Result<Directory>
	where
		P1: AsRef<Path>,
		P2: AsRef<Path>,
	{
		let p = p.as_ref();
		let basepath = basepath.as_ref();
		let name = p
			.file_name()
			.and_then(|name| name.to_str())
			.unwrap_or_else(|| "")
			.to_string();

		Ok(Directory {
			fullpath: basepath.join(&p).canonicalize().unwrap_or_else(|_| basepath.join(&p)),
			path: p.into(),
			name,
			url: None,
			dirs: Vec::new(),
			files: Vec::new(),
		})
	}

	pub fn scan_from<P>(p: P, config: &Config) -> Result<Directory>
	where
		P: AsRef<Path>,
	{
		let p = p.as_ref();

		// Save current dir
		let cur_dir = env::current_dir().or_err(|| "Cannot retrieve current dir")?;

		// Change into the dir to scan
		env::set_current_dir(p).or_err(|| "Cannot chdir into source dir")?;
		let currentdir = env::current_dir().or_err(|| format!("Cannot access dir: {}", p.display()))?;
		let mut res = Directory::new(Path::new("."), &currentdir)?;
		res.discover_children(&currentdir, config)?;

		// Go back to the dir we were before scanning
		env::set_current_dir(&cur_dir).expect("Cannot chdir back to main dir");
		Ok(res)
	}

	fn discover_children<P>(&mut self, basepath: P, config: &Config) -> Result<()>
	where
		P: AsRef<Path>,
	{
		let basepath = basepath.as_ref();

		let paths = fs::read_dir(&self.path).or_err(|| "Unable to open dir")?;
		let mut has_index = false;
		for dir_entry in paths {
			match dir_entry {
				Ok(d) => {
					let path = d.path();
					let path = path.strip_prefix("./").map(|v| v.to_path_buf()).unwrap_or(path);

					// We need valid UTF8 to represent them as UTF8 HTML links
					let _ = path
						.to_str()
						.or_err(|| format!("`{}` is an invalid filename", path.display()))?;

					let file_name = path
						.file_name()
						.and_then(|name| name.to_str())
						.unwrap_or_else(|| "");

					if files::is_filename_not_allowed(file_name) {
						continue;
					}

					if path.is_dir() {
						let mut dir = Directory::new(&path, basepath)?;
						dir.discover_children(basepath, config)
							.or_err(|| format!("Error scanning {}", basepath.display()))?;

						let dir_name = dir.name();
						trace!("Added dir: {}", dir_name);
						self.dirs.push(dir);
					} else {
						let file =
							File::new(&path, basepath).or_err(|| format!("Error scanning {}", &path.display()))?;
						let file_name = file.name();
						has_index = has_index || file.is_index();
						trace!("Added file: {}", file_name);
						self.files.push(file);
					}
				}
				Err(e) => {
					error!("Error: {}", e);
				}
			}
		}

		if !has_index && config.site.indexes {
			self.files.push(File::new_index(&self.path, basepath)?)
		}

		self.dirs.sort_by(|a, b| {
			let a = a.id();
			let b = b.id();
			a.cmp(&b)
		});

		self.files.sort_by(|a, b| {
			let a = a.id();
			let b = b.id();
			a.cmp(&b)
		});

		Ok(())
	}

	pub fn id(&self) -> String {
		format!("dir-{}", &self.fullpath.display())
	}

	pub fn name(&self) -> String {
		self.name.clone()
	}
}

//#[cfg(test)]
//mod tests {
//	use super::*;
//
//	#[test]
//	fn test_scan() {
//		let dir = Directory::scan_from("tests/fixtures").expect("Error scanning");
//		println!("{:?}", dir);
//	}
//}
