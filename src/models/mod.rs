/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

mod directory;
mod file;

pub use self::directory::Directory;
pub use self::file::File;
pub use self::file::FileMeta;

use chrono::{DateTime, Utc, TimeZone};
use serde::ser::{Serialize, Serializer, SerializeStruct};
use serde_json::{Map as JsonMap, Value};

type Object = JsonMap<String, Value>;

#[derive(Debug, Clone)]
pub struct Date {
	tm: DateTime<Utc>,
}

impl Serialize for Date {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where S: Serializer
	{
		// REMEMBER the number of fields in the struct.
		let mut state = serializer.serialize_struct("Date", 18)?;
		state.serialize_field("unix", &self.tm.format("%s").to_string())?;

		state.serialize_field("year", &self.tm.format("%Y").to_string())?;
		state.serialize_field("month", &self.tm.format("%m").to_string())?;
		state.serialize_field("day", &self.tm.format("%d").to_string())?;
		state.serialize_field("hour", &self.tm.format("%H").to_string())?;
		state.serialize_field("minute", &self.tm.format("%M").to_string())?;
		state.serialize_field("second", &self.tm.format("%S").to_string())?;

		state.serialize_field("month_short", &self.tm.format("%b").to_string())?;
		state.serialize_field("month_name", &self.tm.format("%B").to_string())?;

		state.serialize_field("iso", &self.iso())?;
		state.serialize_field("full", &self.full())?;
		state.serialize_field("datetime", &self.datetime())?;
		state.serialize_field("date", &self.date())?;
		state.serialize_field("date_h", &self.date_h())?;
		state.serialize_field("date_hs", &self.date_hs())?;
		state.serialize_field("time", &self.time())?;
		state.serialize_field("fulltime", &self.fulltime())?;

		state.serialize_field("rss", &self.tm.format("%a, %d %b %Y %H:%M:%S %Z").to_string())?;

		state.end()
	}
}

impl Date {
	pub fn new(dt: &str) -> Option<Date> {
		Utc.datetime_from_str(dt, "%Y-%m-%dT%H:%M:%S%z")
			.or_else(|_| Utc.datetime_from_str(dt, "%Y-%m-%dT%H:%M:%S"))
			.or_else(|_| Utc.datetime_from_str(dt, "%Y-%m-%d %H:%M:%S"))
			.or_else(|_| Utc.datetime_from_str(dt, "%Y-%m-%d %H:%M"))
			.or_else(|_| Utc.datetime_from_str(dt, "%Y-%m-%d"))
			.ok()
			.map(|v| Date { tm: v })
	}

	pub fn iso(&self) -> String {
		self.tm.format("%FT%H:%M:%SZ").to_string()
	}

	pub fn datetime(&self) -> String {
		self.tm.format("%F %H:%M").to_string()
	}

	pub fn full(&self) -> String {
		self.tm.format("%F %H:%M:%S").to_string()
	}

	pub fn date(&self) -> String {
		self.tm.format("%F").to_string()
	}

	pub fn date_h(&self) -> String {
		self.tm.format("%e %B %Y").to_string()
	}

	pub fn date_hs(&self) -> String {
		self.tm.format("%e %b %Y").to_string()
	}

	pub fn fulltime(&self) -> String {
		self.tm.format("%H:%M:%S").to_string()
	}

	pub fn time(&self) -> String {
		self.tm.format("%H:%M").to_string()
	}
}

impl From<DateTime<Utc>> for Date {
	fn from(t: DateTime<Utc>) -> Date {
		Date { tm: t}
	}
}
