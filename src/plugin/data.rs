/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;

use serde_yaml::from_str as yaml_from_str;

use std::fs;
use std::path::Path;

use super::{Object, Plugin, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::Directory;
use crate::utils::files;

pub struct DataPlugin;

impl Plugin for DataPlugin {
	fn name(&self) -> String {
		"Data".to_string()
	}

	fn config_defaults(&self) -> Option<Object> {
		let mut defaults = Object::new();
		defaults.insert("dir".into(), "data".into());
		Some(defaults)
	}

	fn post_init(&self, config: &Config, base_dir: &Path) -> PluginResult {
		let full_path = base_dir.join(
			&config
				.plugins
				.get_string(&self.id(), "dir")
				.unwrap_or_else(|| "data".to_string()),
		);

		if full_path.exists() {
			return Ok(Some("Data dir already exists".into()));
		}

		fs::create_dir_all(&full_path).or_err(|| {
			format!(
				"Unable to create requested data directory '{}'",
				full_path.to_str().unwrap_or_else(|| "[invalid filename]")
			)
		})?;

		Ok(Some("Data dir created".into()))
	}

	fn before_render(&self, config: &Config, _: &Directory) -> PluginResult {
		let data_dir = config.paths.root().join("data");

		if !data_dir.exists() || !data_dir.is_dir() {
			warn!("Path {} does not exist", data_dir.to_str().unwrap());
			return Ok(None);
		}

		let mut data = Object::new();

		let paths = fs::read_dir(&data_dir).or_err(|| "Cannot read dir")?;
		let mut unknowns = 0;
		for dir_entry in paths {
			match dir_entry {
				Ok(d) => {
					let path = d.path();

					if path.is_dir() {
						continue;
					}

					let file_name = match path.file_name() {
						Some(n) => match n.to_str() {
							Some(n) => n.to_string(),
							None => "".to_string(),
						},
						None => "".to_string(),
					};

					if file_name == "" || file_name.starts_with('.') {
						continue;
					}

					let extension = match path.extension() {
						Some(e) => e.to_str().unwrap_or_else(|| "").to_string(),
						None => "".to_string(),
					};

					let base_name = match path.file_stem() {
						Some(n) => n.to_str().unwrap_or_else(|| "decoding_error").to_string(),
						None => {
							unknowns += 1;
							format!("unknown_{}", unknowns).to_string()
						}
					};

					if extension == "yml" || extension == "yaml" {
						let file_data = yaml_from_str::<Value>(&files::read_to_string(&path)?)
							.or_err(|| format!("Unable to load YAML from {}", &path.display()))?;
						let file_data = file_data
							.as_object()
							.ok_or_else(|| Error::new("Data YAML must represent an object"))?;

						data.insert(base_name, Value::Object(file_data.to_owned()));
					} else {
						continue;
					}
				}
				Err(e) => {
					error!("Error: {}", e);
				}
			}
		}

		Ok(Some(Value::Object(data)))
	}
}
