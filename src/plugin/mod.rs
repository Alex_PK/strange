/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

mod config;
mod data;
mod nav;
mod redirect;
mod rss;
mod sitemap;
mod tag;
mod theme;
mod types;

use rayon::prelude::*;
use serde::ser::{Serialize, Serializer};
use serde_json::{Map as JsonMap, Value};
use std::collections::BTreeMap;
use std::fmt::{Debug, Formatter, Result as FmtResult};
use std::path::Path;
use time;

use crate::config::Config;
use crate::errors::*;
use crate::models::Directory;
use crate::utils::print;

pub type Object = JsonMap<String, Value>;
pub type PluginResult = Result<Option<Value>>;
pub struct PluginsData(BTreeMap<String, PluginResult>);
pub type PluginsResult = Result<PluginsData>;

pub trait PluginPageData {
	fn to_object(&self) -> Object;
}

impl Serialize for PluginPageData + Send + Sync {
	fn serialize<S>(&self, serializer: S) -> ::std::result::Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		let o = self.to_object();
		o.serialize(serializer)
	}
}

impl Debug for PluginPageData + Send + Sync {
	fn fmt(&self, fmt: &mut Formatter) -> FmtResult {
		write!(fmt, "PluginPageData{{{:?}}}", self.to_object())
	}
}

impl PluginsData {
	pub fn get_data(self) -> Object {
		debug!("Collecting plugins data");
		self.0
			.into_iter()
			.filter_map(|(idx, elem)| match elem {
				Err(e) => {
					dump_error(&e);
					None
				}
				Ok(e) => match e {
					Some(e) => Some((idx, e)),
					None => None,
				},
			})
			.collect()
	}
}

impl From<BTreeMap<String, PluginResult>> for PluginsData {
	fn from(s: BTreeMap<String, PluginResult>) -> PluginsData {
		PluginsData(s)
	}
}

pub struct Plugins(Vec<Box<Plugin + Send + Sync>>);

impl Plugins {
	pub fn new() -> Result<Plugins> {
		let mut plugin_list: Vec<Box<Plugin + Send + Sync>> = Vec::new();

		plugin_list.push(Box::new(config::ConfigPlugin));
		plugin_list.push(Box::new(data::DataPlugin));
		plugin_list.push(Box::new(sitemap::SitemapPlugin));
		plugin_list.push(Box::new(nav::NavPlugin));
		plugin_list.push(Box::new(tag::TagPlugin));
		plugin_list.push(Box::new(redirect::RedirectPlugin));
		plugin_list.push(Box::new(rss::RssPlugin));
		plugin_list.push(Box::new(types::TypePlugin));
		plugin_list.push(Box::new(theme::ThemePlugin));

		Ok(Plugins(plugin_list))
	}

	pub fn config_defaults(&self) -> BTreeMap<String, Object> {
		self.0
			.iter()
			.filter_map(|plugin| {
				if let Some(config) = plugin.config_defaults() {
					Some((plugin.id(), config))
				} else {
					None
				}
			})
			.collect()
	}

	pub fn post_init(&self, config: &Config, base_dir: &Path) -> PluginsResult {
		let results: BTreeMap<String, PluginResult> = self
			.0
			.par_iter()
			.map(|plugin| {
				trace!("Init Processing {}", plugin.id());
				let ts_start = time::get_time();
				let res = (plugin.id(), plugin.post_init(config, base_dir));
				let ts_end = time::get_time();

				print::tprofiling(&format!("Processing {}", plugin.id()), &ts_start, &ts_end);

				res
			})
			.collect();

		Ok(results.into())
	}

	pub fn before_render(&self, config: &Config, index: &Directory) -> PluginsResult {
		let results: BTreeMap<String, PluginResult> = self
			.0
			.par_iter()
			.map(|plugin| {
				trace!("Processing {} plugin", plugin.id());

				let ts_start = time::get_time();
				let res = (plugin.id(), plugin.before_render(config, index));
				let ts_end = time::get_time();

				print::tprofiling(&format!("Processing {}", plugin.id()), &ts_start, &ts_end);

				res
			})
			.collect();

		Ok(results.into())
	}

	pub fn after_render(&self, config: &Config, index: &Directory) -> PluginsResult {
		let results: BTreeMap<String, PluginResult> = self
			.0
			.par_iter()
			.map(|plugin| {
				trace!("Post Processing {}", plugin.id());

				let ts_start = time::get_time();
				let res = (plugin.id(), plugin.after_render(config, index));
				let ts_end = time::get_time();

				print::tprofiling(&format!("Processing {}", plugin.id()), &ts_start, &ts_end);

				res
			})
			.collect();

		Ok(results.into())
	}
}

pub trait Plugin {
	fn name(&self) -> String;

	fn id(&self) -> String {
		self.name().to_lowercase()
	}

	fn config_defaults(&self) -> Option<Object> {
		None
	}

	fn post_init(&self, _: &Config, _: &Path) -> PluginResult {
		Ok(None)
	}

	fn before_render(&self, _: &Config, _: &Directory) -> PluginResult {
		Ok(None)
	}

	fn after_render(&self, _: &Config, _: &Directory) -> PluginResult {
		Ok(None)
	}
}
