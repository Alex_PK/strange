/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::{to_value, Map as JsonMap, Value};
use std::collections::HashMap;
use std::sync::Arc;

type Object = JsonMap<String, Value>;

use super::{Plugin, PluginPageData, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::{Directory, FileMeta};

pub struct NavPluginData {
	prev: Option<Arc<FileMeta>>,
	next: Option<Arc<FileMeta>>,
	pages: Arc<Vec<Arc<FileMeta>>>,
	dirs: Arc<HashMap<String, Arc<FileMeta>>>,
	files: Arc<Vec<Arc<FileMeta>>>,
}

impl PluginPageData for NavPluginData {
	fn to_object(&self) -> Object {
		let mut o = Object::new();
		if let Some(ref prev) = self.prev {
			o.insert(
				"prev".into(),
				Value::Object(to_value(prev).unwrap().as_object().unwrap().to_owned()),
			);
		}
		if let Some(ref next) = self.next {
			o.insert(
				"next".into(),
				Value::Object(to_value(next).unwrap().as_object().unwrap().to_owned()),
			);
		}
		o.insert(
			"pages".into(),
			Value::Array(to_value(&self.pages).unwrap().as_array().unwrap().to_owned()),
		);
		o.insert(
			"dirs".into(),
			Value::Object(to_value(&self.dirs).unwrap().as_object().unwrap().to_owned()),
		);
		o.insert(
			"files".into(),
			Value::Array(to_value(&self.files).unwrap().as_array().unwrap().to_owned()),
		);

		o
	}
}

pub struct NavPlugin;

impl Plugin for NavPlugin {
	fn name(&self) -> String {
		"Nav".to_string().clone()
	}

	fn before_render(&self, _: &Config, index: &Directory) -> PluginResult {
		self.process_all(index)?;

		let mut res = Vec::new();
		self.process_flat(index, &mut res)?;
		Ok(Some(Value::Array(res)))
	}
}

impl NavPlugin {
	fn process_all(&self, index: &Directory) -> Result<()> {
		let files_len = index.files.len();

		// Get a list of the pages at the same level
		let mut pages = Vec::new();
		let mut files = Vec::new();
		for file in &index.files {
			if file.is_page_or_index() {
				pages.push(file.get_meta());
			} else {
				files.push(file.get_meta());
			}
		}
		let pages = Arc::new(pages);
		let files = Arc::new(files);

		// Get a list of the dirs at the same level
		let mut dirs = HashMap::new();
		for dir in &index.dirs {
			if let Some(file) = dir
				.files
				.iter()
				.find(|f| {
					trace!("Checking {}, {}", index.path.display(), f.path.display());
					f.is_index() || f.is_generated_index()
				})
				.map(|f| f.get_meta())
			{
				trace!("Found {:?}", file);
				dirs.insert(dir.name(), file);
			}
		}
		let dirs = Arc::new(dirs);

		for n in 0..files_len {
			let mut prev_meta = None;
			let mut next_meta = None;

			let (prev_vec, rest) = index.files.split_at(n);

			let (current, next_vec) = rest
				.split_first()
				.or_err(|| "Unable to isolate first value")?;

			if !current.is_page_or_index() {
				continue;
			}

			if n > 0 {
				let (prev, _) = prev_vec
					.split_last()
					.or_err(|| "Cannot get prev value")?;

				if prev.is_page_or_index() {
					// TODO: look for a doc in the prev list instead of ignoring them
					prev_meta = Some(prev.get_meta());
				}
			}

			if n < (files_len - 1) {
				let (next, _) = next_vec
					.split_first()
					.or_err(|| "Cannot get next value")?;

				if next.is_page_or_index() {
					// TODO: look for a doc in the prev list instead of ignoring them
					next_meta = Some(next.get_meta());
				}
			}
			current.add_page_data(
				&self.id(),
				Box::new(NavPluginData {
					prev: prev_meta,
					next: next_meta,
					pages: pages.clone(),
					dirs: dirs.clone(),
					files: files.clone(),
				}),
			);
		}

		for dir in &index.dirs {
			self.process_all(dir)?;
		}

		Ok(())
	}

	fn process_flat(&self, index: &Directory, res: &mut Vec<Value>) -> Result<()> {
		for file in &index.files {
			res.push(Value::Object(file.get_meta().to_object()));
		}

		for dir in &index.dirs {
			self.process_flat(dir, res)?;
		}

		Ok(())
	}
}
