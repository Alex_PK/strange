/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::{Object, Plugin, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::Directory;
use crate::utils::files;

pub struct RedirectPlugin;

impl Plugin for RedirectPlugin {
	fn name(&self) -> String {
		"Redirect".to_string()
	}

	fn config_defaults(&self) -> Option<Object> {
		let mut defaults = Object::new();
		defaults.insert("enabled".into(), true.into());
		defaults.insert("status".into(), 301.into());
		Some(defaults)
	}

	fn after_render(&self, config: &Config, index: &Directory) -> PluginResult {
		if !config.plugins.get_bool(&self.id(), "enabled").unwrap_or(true) {
			return Ok(None);
		}

		let status_code = config.plugins.get_int(&self.id(), "status").unwrap_or(301);

		let mut redirectlist = Vec::new();
		self.build_pagelist(index, &mut redirectlist)?;
		let dest_path = config.paths.dest().join(".htaccess");

		let redirects = redirectlist
			.iter()
			.map(|&(ref from, ref to)| format!("\nRedirect {} \"{}\" \"{}\"", &status_code, &from, &to))
			.collect::<String>();

		trace!("Redirects: {}\n-- Writing to {}", &redirects, &dest_path.display());

		if redirects != "" {
			files::write_to_file(&dest_path, &redirects)?;
		}

		Ok(None)
	}
}

impl RedirectPlugin {
	fn build_pagelist(&self, index: &Directory, redirectlist: &mut Vec<(String, String)>) -> Result<()> {
		for file in &index.files {
			if file.is_page_or_index() {
				let meta = file.get_meta();
				meta.extra.get("redirect_from").and_then(|v| {
					if let Some(v) = v.as_str() {
						redirectlist.push((v.to_owned(), meta.url.clone()));
					};
					Some(())
				});
			}
		}

		for dir in &index.dirs {
			self.build_pagelist(dir, redirectlist)?;
		}
		Ok(())
	}
}
