/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::{to_value, Value};
use std::collections::BTreeMap;
use std::sync::Arc;
use crate::tpl::engine_tera::Tera;

use super::{Object, Plugin, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::{Directory, FileMeta};
use crate::utils::files;

pub struct RssPlugin;

impl Plugin for RssPlugin {
	fn name(&self) -> String {
		"Rss".to_string()
	}

	fn config_defaults(&self) -> Option<Object> {
		let mut defaults = Object::new();
		defaults.insert("enabled".into(), true.into());
		defaults.insert("filename".into(), "rss.xml".into());
		defaults.insert("title".into(), "RSS feed for {title}".into());
		defaults.insert("num".into(), 10.into());
		defaults.insert("type".into(), "*".into());
		Some(defaults)
	}

	fn before_render(&self, config: &Config, _: &Directory) -> PluginResult {
		let mut data = Object::new();

		let mut title = config.plugins.get_string(&self.id(), "title").unwrap_or_default();
		if title.is_empty() {
			title = "{title}".into();
		}
		title = title.replace("{title}", &config.site.title);

		let url = format!(
			"{}/{}",
			&config.site.url,
			&config
				.plugins
				.get_string(&self.id(), "filename")
				.unwrap_or_else(|| "rss.xml".into())
		);

		let rel_link = format!(
			r##"<link rel="alternate" type="application/rss+xml" title="{}" href="{}" />"##,
			&title, &url,
		);

		data.insert("url".into(), Value::String(url));
		data.insert("rel_link".into(), Value::String(rel_link));

		Ok(Some(Value::Object(data)))
	}

	fn after_render(&self, config: &Config, index: &Directory) -> PluginResult {
		if !config.plugins.get_bool(&self.id(), "enabled").unwrap_or(true) {
			return Ok(None);
		}

		let types = config.plugins.get_string(&self.id(), "type").and_then(|t| {
			if t == "*" {
				debug!("Generating RSS for all pages");
				None
			} else {
				let res = t.split('|').map(|v| v.to_string()).collect::<Vec<_>>();
				debug!("Generating RSS for {}", res.join(", "));
				Some(res)
			}
		});

		let mut pagelist = BTreeMap::new();
		self.build_pagelist(index, &mut pagelist, &types)?;
		let mut posts = pagelist.iter().flat_map(|(_, el)| el.iter()).collect::<Vec<_>>();

		posts.sort_by_key(|el| el.date.iso());

		let posts = posts
			.iter()
			.rev()
			.take(config.plugins.get_int(&self.name(), "num").unwrap_or(10) as usize)
			.filter_map(|el| {
				to_value(el)
					.ok()
					.and_then(|v| v.as_object().cloned())
					.map(Value::Object)
			})
			.collect::<Vec<_>>();

		let dest_path = config.paths.dest().join(
			&config
				.plugins
				.get_string(&self.id(), "filename")
				.unwrap_or_else(|| "rss.xml".into()),
		);

		let mut data = Object::new();
		data.insert("title".into(), config.site.title.clone().into());
		data.insert("url".into(), config.site.url.clone().into());
		data.insert("description".into(), config.site.description.clone().into());
		data.insert("posts".into(), posts.into());
		let rss_string = Tera::new("").and_then(|tera| tera.render(include_str!("files/rss.xml.twig"), &data))?;

		files::write_to_file(&dest_path, &rss_string)?;

		Ok(None)
	}
}

impl RssPlugin {
	fn build_pagelist(
		&self,
		index: &Directory,
		pagelist: &mut BTreeMap<String, Vec<Arc<FileMeta>>>,
		types: &Option<Vec<String>>,
	) -> Result<()> {
		for file in &index.files {
			if file.is_page() {
				let meta = file.get_meta();
				let dt = meta.date.iso();
				let to_add: bool = if let Some(ref l) = *types {
					l.contains(&meta.ty)
				} else {
					true
				};
				if to_add {
					let list = pagelist.entry(dt).or_insert_with(Vec::new);
					list.push(meta);
				}
			}
		}

		for dir in &index.dirs {
			self.build_pagelist(dir, pagelist, types)?;
		}
		Ok(())
	}
}
