/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;

use super::{Object, Plugin, PluginResult};
use crate::config::Config;
use crate::models::Directory;

pub struct SitemapPlugin;

impl Plugin for SitemapPlugin {
	fn name(&self) -> String {
		"Sitemap".to_string()
	}

	fn before_render(&self, _: &Config, index: &Directory) -> PluginResult {
		Ok(Some(Value::Object(self.build_result(index))))
	}
}

impl SitemapPlugin {
	fn build_result(&self, index: &Directory) -> Object {
		let mut pages = Vec::new();
		let mut files = Vec::new();
		let mut dirs = Object::new();

		for file in &index.files {
			if file.is_page_or_index() {
				pages.push(Value::Object(file.get_meta().to_object()));
			} else {
				files.push(Value::Object(file.get_meta().to_object()));
			}
		}

		for dir in &index.dirs {
			dirs.insert(dir.name(), Value::Object(self.build_result(dir)));
		}

		let mut res = Object::new();
		res.insert("pages".to_string(), Value::Array(pages.clone()));
		res.insert("files".to_string(), Value::Array(files.clone()));
		res.insert("dirs".to_string(), Value::Object(dirs));

		res
	}
}
