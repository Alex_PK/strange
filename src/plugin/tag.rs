/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::{to_value, Value};
use std::collections::BTreeMap;

use super::{Plugin, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::Directory;

pub struct TagPlugin;

impl Plugin for TagPlugin {
	fn name(&self) -> String {
		"Tag".to_string()
	}

	fn before_render(&self, _: &Config, index: &Directory) -> PluginResult {
		let mut taglist: BTreeMap<String, Vec<Value>> = BTreeMap::new();
		self.build_taglist(index, &mut taglist)?;
		let taglist = to_value(taglist)
			.or_err(|| "Cannot serialize tag")?
			.as_object()
			.map(|o| o.to_owned())
			.or_err(|| "Unable to serialize tags")?;

		Ok(Some(Value::Object(taglist)))
	}
}

impl TagPlugin {
	fn build_taglist(&self, index: &Directory, taglist: &mut BTreeMap<String, Vec<Value>>) -> Result<()> {
		for file in &index.files {
			if file.is_page_or_index() {
				for tag in &file.get_meta().tags {
					let a = taglist.entry(tag.to_owned()).or_insert_with(Vec::new);
					let meta = file.get_meta().to_object();
					(*a).push(Value::Object(meta));
				}
			}
		}

		for dir in &index.dirs {
			self.build_taglist(dir, taglist)?;
		}

		Ok(())
	}
}
