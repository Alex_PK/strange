/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::{to_value, Value};
use std::collections::BTreeMap;

use super::{Plugin, PluginResult};
use crate::config::Config;
use crate::errors::*;
use crate::models::Directory;

pub struct TypePlugin;

impl Plugin for TypePlugin {
	fn name(&self) -> String {
		"Type".to_string()
	}

	fn before_render(&self, _: &Config, index: &Directory) -> PluginResult {
		let mut typelist: BTreeMap<String, Vec<Value>> = BTreeMap::new();
		self.build_typelist(index, &mut typelist)?;
		let typelist = to_value(typelist)
			.or_err(|| "Cannot serialize types")?
			.as_object()
			.map(|o| o.to_owned())
			.or_err(|| "Unable to serialize types")?;

		Ok(Some(Value::Object(typelist)))
	}
}

impl TypePlugin {
	fn build_typelist(&self, index: &Directory, typelist: &mut BTreeMap<String, Vec<Value>>) -> Result<()> {
		for file in &index.files {
			if file.is_page_or_index() {
				let meta = file.get_meta();
				let a = typelist.entry(meta.ty.to_owned()).or_insert_with(Vec::new);
				let meta = file.get_meta().to_object();
				(*a).push(Value::Object(meta));
			}
		}

		for dir in &index.dirs {
			self.build_typelist(dir, typelist)?;
		}

		Ok(())
	}
}
