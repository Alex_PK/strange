/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use serde_json::Value;
use std::collections::BTreeMap;
use std::fs;
use std::path::{Path, PathBuf};

use crate::config::Config;
use crate::errors::*;
use crate::plugin::Object;
use serde_yaml;
use crate::tpl::engine_tera::Tera;
use crate::utils::files;

pub struct Themes {
	dir: PathBuf,
	dest: PathBuf,
	current: String,
	themes: BTreeMap<String, Theme>,
}

impl Themes {
	pub fn new(config: &Config) -> Result<Themes> {
		let dir = config.paths.themes();

		let paths = fs::read_dir(&dir).or_err(|| (format!("Unable to access {:?}", dir), ErrorKind::ThemeEngine))?;

		let current = config.theme.name.clone();
		let mut themes = BTreeMap::new();

		paths
			.filter_map(|e| e.ok())
			.map(|d| d.path())
			.filter(|p| p.is_dir())
			.for_each(|p| if let Ok(theme) = Theme::new(&p, config) {
				let name = theme.name.to_owned();
				debug!("Found theme `{}`", &name);
				themes.insert(name, theme);
			});

		if themes.get(&current).is_none() {
			warn!("The theme `{}`, selected as current, is not available", &current);
		}

		Ok(Themes {
			dir,
			dest: config.paths.dest().join(&config.theme.dest),
			current,
			themes,
		})
	}

	pub fn get_current(&self) -> Option<&Theme> {
		self.themes.get(&self.current)
	}

	pub fn get_src(&self) -> &PathBuf {
		&self.dir
	}

	pub fn get_dest(&self) -> &PathBuf {
		&self.dest
	}
}

pub struct Theme {
	tera: Tera,
	extensions: Vec<&'static str>,
	src_dir: PathBuf,
	dest_path: PathBuf,
	pub name: String,
	theme: Object,
	pub meta: Meta,
}

impl Theme {
	pub fn new<P>(path: P, config: &Config) -> Result<Theme>
	where
		P: AsRef<Path>,
	{
		let theme_dir = path.as_ref();
		let theme_name = theme_dir
			.file_name()
			.and_then(|n| n.to_str())
			.or_err(|| "Invalid directory name")?;

		let dest_path = config.paths.dest().join(&config.theme.dest);

		let src_dir = theme_dir.join("src");

		let meta = read_meta(&theme_dir);

		let mut theme = Object::new();
		theme.insert("name".into(), Value::String(config.theme.name.clone()));
		theme.insert("options".into(), Value::Object(config.theme.options.clone()));
		theme.insert("data".into(), Value::Object(config.theme.data.clone()));

		Ok(Theme {
			tera: Tera::new("").or_err(|| {
				(
					"Error getting rendering engine while parsing theme",
					ErrorKind::Theme(theme_name.to_owned()),
				)
			})?,
			extensions: Tera::extensions(),
			src_dir,
			dest_path,
			name: theme_name.to_owned(),
			theme,
			meta,
		})
	}

	pub fn render(&self, plugin_results: &Object) -> Result<()> {
		self.render_dir(&self.src_dir, &self.dest_path, plugin_results)
	}

	fn render_dir<P1, P2>(&self, src: P1, dst: P2, plugin_results: &Object) -> Result<()>
	where
		P1: AsRef<Path>,
		P2: AsRef<Path>,
	{
		let src = src.as_ref();
		let dst = dst.as_ref();
		let paths = fs::read_dir(src).or_err(|| {
			(
				format!("Unable to access {:?} while rendering theme", src),
				ErrorKind::ThemeEngine,
			)
		})?;

		let paths = paths
			.filter_map(|e| e.ok())
			.map(|d| d.path())
			.filter(|p| !files::is_filename_not_allowed(p.file_name().and_then(|s| s.to_str()).unwrap_or_default()));

		for path in paths {
			let file_name = path.file_name().and_then(|s| s.to_str()).unwrap_or_default();

			if path.is_dir() {
				let mut dst = dst.to_path_buf();
				dst.push(&file_name);
				fs::create_dir_all(&dst).or_err(|| "Cannot create destination directory")?;
				self.render_dir(&path, &dst, plugin_results).or_err(|| {
					(
						format!("Error while scanning directory {} for theme rendering", path.display()),
						ErrorKind::ThemeEngine,
					)
				})?;
			} else {
				let mut contents = files::read_to_string(&path)?;

				let mut meta = Object::new();
				meta.insert("name".into(), file_name.into());
				meta.insert(
					"local_path".into(),
					path.parent().and_then(|p| p.to_str()).unwrap_or_default().into(),
				);
				meta.insert("site".into(), Value::Object(plugin_results.to_owned()));
				meta.insert("theme".into(), Value::Object(self.theme.clone()));

				let ext = path.extension().and_then(|s| s.to_str()).unwrap_or_default();

				let mut dst = dst.to_path_buf();

				if self.extensions.contains(&ext) {
					contents = self
						.tera
						.render_body(&contents, &meta)
						.or_err(|| (format!("Error rendering {}", path.display()), ErrorKind::ThemeEngine))?;

					let stem = path.file_stem().and_then(|s| s.to_str()).unwrap_or_default();
					if stem == "" {
						continue;
					}

					dst.push(&stem);
				} else {
					dst.push(&file_name);
				}

				files::write_to_file(dst, &contents).or_err(|| {
					(
						format!("Error writing {} to disk", path.display()),
						ErrorKind::ThemeEngine,
					)
				})?;
			}
		}

		Ok(())
	}
}

pub fn read_meta<P>(path: P) -> Meta
where
	P: AsRef<Path>,
{
	let path = path.as_ref().join("theme.yaml");
	fs::File::open(&path)
		.map_err(|_| warn!("theme.yaml not found"))
		.and_then(|f| serde_yaml::from_reader(&f).map_err(|_| error!("Invalid file format for theme.yaml")))
		.unwrap_or_else(|_| Meta::new())
}

#[derive(Serialize, Deserialize, Default, Debug, Clone)]
pub struct Meta {
	#[serde(default = "dfl_unknown")]
	pub name: String,

	#[serde(default = "dfl_unknown")]
	pub author: String,

	#[serde(default = "dfl_unknown")]
	pub version: String,
}

impl Meta {
	fn new() -> Meta {
		Meta {
			name: dfl_unknown(),
			author: dfl_unknown(),
			version: dfl_unknown(),
		}
	}
}

pub fn dfl_unknown() -> String {
	"Unknown".into()
}
