/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use handlebars;
use serde_json::{Map as JsonMap, Value};
use std::fs;
use std::path::Path;
use std::sync::RwLock;

use crate::errors::*;
use crate::utils::files;

type Object = JsonMap<String, Value>;

pub struct Handlebars {
	engine: RwLock<handlebars::Handlebars>,
}

impl Handlebars {
	pub fn extensions() -> Vec<&'static str> {
		vec!["hbs"]
	}

	pub fn new<P>(tpl_path: P) -> Result<Handlebars>
	where
		P: AsRef<Path>,
	{
		let tpl_path = tpl_path.as_ref();

		let mut handlebars = handlebars::Handlebars::new();

		let extensions = Self::extensions();
		if tpl_path.exists() {
			for dir_entry in fs::read_dir(&tpl_path).unwrap() {
				if let Ok(ref d) = dir_entry {
					let path = d.path();

					let ext = path.extension().and_then(|f| f.to_str()).unwrap_or_default();
					let file_name = path.file_stem().and_then(|f| f.to_str()).unwrap_or_default().to_owned();
					if file_name.is_empty() || !extensions.contains(&ext) {
						continue;
					}

					trace!("Adding Handlebars template {:?}", file_name);
					let tpl = files::read_to_string(&path).or_err(|| {
						(
							format!("Unable to load template '{}'", file_name),
							ErrorKind::TemplateEngine("Handlebar".into()),
						)
					})?;

					handlebars.register_template_string(&file_name, tpl).or_err(|| {
						(
							format!("Unable to register template '{}'", file_name),
							ErrorKind::TemplateEngine("Handlebar".into()),
						)
					})?;
				}
			}
		}

		// Disable automatic escaping, as we don't know if the rendered data will be HTML or not
		handlebars.register_escape_fn(handlebars::no_escape);

		Ok(Handlebars {
			engine: RwLock::new(handlebars),
		})
	}

	pub fn render_caption(&self, body: &str, data: &Object) -> Result<String> {
		self.engine.read().unwrap().render_template(body, data).or_err(|| {
			(
				"Unable to render caption",
				ErrorKind::TemplateEngine("Handlebar".into()),
			)
		})
	}

	pub fn render_body(&self, body: &str, data: &Object) -> Result<String> {
		self.engine
			.read()
			.unwrap()
			.render_template(body, data)
			.or_err(|| ("Unable to render body", ErrorKind::TemplateEngine("Handlebar".into())))
	}

	pub fn render_page(&self, tpl_type: &str, data: &Object) -> Result<String> {
		self.engine.read().unwrap().render(tpl_type, data).or_err(|| {
			(
				format!("Unable to render template '{}'", tpl_type),
				ErrorKind::TemplateEngine("Handlebar".into()),
			)
		})
	}

	pub fn has_template(&self, tpl_type: &str) -> bool {
		self.engine.read().unwrap().get_template(tpl_type).is_some()
	}

	pub fn get_page_src(&self, title: &str, dt: &str, typ: &str) -> String {
		format!(
			include_str!("files/page.md.handlebars"),
			title = title,
			date = dt,
			typ = typ
		)
	}
}
