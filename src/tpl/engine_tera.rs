/*
	Copyright 2017-2019 Alessandro Pellizzari

	This file is part of strange.

	Strange is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Strange is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Strange.  If not, see <http://www.gnu.org/licenses/>.
*/

use regex::Regex;
use serde::Serialize;
use serde_json::{
	from_value as json_decode, to_string as json_encode, to_string_pretty as json_encode_pretty, Map as JsonMap, Value,
};
use serde_yaml::to_string as yaml_encode;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::RwLock;
use std::{cmp, fs};
use tera;

use crate::errors::*;
use crate::utils::files;

type Object = JsonMap<String, Value>;

pub struct Tera {
	engine: RwLock<tera::Tera>,
}

impl Tera {
	pub fn extensions() -> Vec<&'static str> {
		vec!["tera", "twig", "j2", "jinja2"]
	}

	pub fn new<P>(tpl_path: P) -> Result<Tera>
	where
		P: AsRef<Path>,
	{
		let tpl_path = tpl_path.as_ref();
		let mut tera = Self::get_tera()?;

		let extensions = Self::extensions();
		if tpl_path != Path::new("") && tpl_path.exists() {
			let mut to_add = HashMap::new();

			for dir_entry in fs::read_dir(&tpl_path).unwrap() {
				if let Ok(ref d) = dir_entry {
					let path = d.path();

					let ext = path.extension().and_then(|f| f.to_str()).unwrap_or_default();
					let file_name = path.file_stem().and_then(|f| f.to_str()).unwrap_or_default().to_owned();
					if file_name.is_empty() || !extensions.contains(&ext) {
						continue;
					}

					trace!("Adding Tera template {:?}", file_name);
					to_add.insert(file_name.clone(), path.clone());
				}
			}

			tera.add_template_files(to_add.iter().map(|(n, p)| (p, Some(n.as_str()))).collect())
				.or_err(|| ("Cannot add template", ErrorKind::TemplateEngine("Tera".into())))?;
		}

		Ok(Tera {
			engine: RwLock::new(tera),
		})
	}

	pub fn get_tera() -> Result<tera::Tera> {
		let mut tera = tera::Tera::default();

		// Disable automatic escaping, as we don't know if the rendered data will be HTML or not
		tera.autoescape_on(vec![]);

		tera.register_filter("debug", filter_debug);
		tera.register_filter("take", filter_take);
		tera.register_filter("sort_objects", filter_sort_objects);
		tera.register_filter("file_dir", filter_file_dir);
		tera.register_filter("file_name", filter_file_name);
		tera.register_filter("file_stem", filter_file_stem);
		tera.register_filter("path_join", filter_path_join);
		tera.register_filter("read_file", filter_read_file);
		tera.register_filter("json_encode", filter_json_encode);
		tera.register_filter("yaml_encode", filter_yaml_encode);
		tera.register_filter("trim", filter_trim);
		tera.register_filter("ltrim", filter_trim_left);
		tera.register_filter("rtrim", filter_trim_right);
		tera.register_filter("explode", filter_explode);

		tera.register_tester("starting_with", tester_starting_with);
		tera.register_tester("ending_with", tester_ending_with);
		tera.register_tester("matching", tester_matching);
		tera.register_tester("with_key", tester_with_key);
		tera.register_tester("containing", tester_containing);

		tera.register_function("dir_scan", make_fn_dir_scan());

		Ok(tera)
	}

	pub fn render<S: Serialize>(&self, body: &str, data: &S) -> Result<String> {
		let mut tera = Self::get_tera()?;
		tera.add_raw_template("_body", body).or_err(|| ("Unable to parse body", ErrorKind::TemplateEngine("Tera".into())))?;

		tera.render("_body", data).or_err(|| ("Unable to render body", ErrorKind::TemplateEngine("Tera".into())))
	}

	pub fn render_caption(&self, body: &str, data: &Object) -> Result<String> {
		self.render(body, data)
	}

	pub fn render_body(&self, body: &str, data: &Object) -> Result<String> {
		self.render(body, data)
	}

	pub fn render_page(&self, tpl_type: &str, data: &Object) -> Result<String> {
		self.engine
			.read()
			.unwrap()
			.render(&tpl_type, data)
			.or_err(|| (format!("Unable to render template '{}'", tpl_type), ErrorKind::TemplateEngine("Tera".into())))
	}

	pub fn has_template(&self, tpl_type: &str) -> bool {
		self.engine.read().unwrap().templates.contains_key(tpl_type)
	}

	pub fn get_page_src(&self, title: &str, dt: &str, typ: &str) -> String {
		format!(include_str!("files/page.md.twig"), title = title, date = dt, typ = typ)
	}
}

fn filter_debug(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	Ok(Value::String(format!("{:?} {:?}", value, args)))
}

fn filter_take(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	if let Some(arr) = value.as_array() {
		if let Some(n) = args.get("n").and_then(|n| n.as_u64()) {
			let from = ::std::cmp::min(
				args.get("from").and_then(|n| n.as_u64()).unwrap_or(0) as usize,
				arr.len(),
			);
			let n = ::std::cmp::min(n as usize, arr.len() - from);
			return Ok(Value::Array(arr[from..(from + n)].to_vec()));
		}
	}

	Ok(value)
}

fn filter_file_dir(value: Value, _: HashMap<String, Value>) -> tera::Result<Value> {
	let path = Path::new(value.as_str().unwrap_or_default());
	let dir = path.parent().and_then(|p| p.to_str()).unwrap_or_default();
	Ok(Value::String(dir.into()))
}

fn filter_file_name(value: Value, _: HashMap<String, Value>) -> tera::Result<Value> {
	let path = Path::new(value.as_str().unwrap_or_default());
	let name = path.file_name().and_then(|p| p.to_str()).unwrap_or_default();
	Ok(Value::String(name.into()))
}

fn filter_file_stem(value: Value, _: HashMap<String, Value>) -> tera::Result<Value> {
	let path = Path::new(value.as_str().unwrap_or_default());
	let dir = path.file_stem().and_then(|p| p.to_str()).unwrap_or_default();
	Ok(Value::String(dir.into()))
}

fn filter_path_join(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let path = PathBuf::from(value.as_str().unwrap_or_default());
	let path = args
		.get("name")
		.and_then(|d| json_decode::<String>(d.clone()).ok())
		.and_then(|part| Some(path.join(part)))
		.unwrap_or(path);

	Ok(Value::from(path.to_str().unwrap_or_default().to_string()))
}

fn filter_json_encode(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let pretty = args.get("pretty").and_then(|v| v.as_bool()).unwrap_or(false);
	if pretty {
		Ok(Value::String(json_encode_pretty(&value)?))
	} else {
		Ok(Value::String(json_encode(&value)?))
	}
}

fn filter_sort_objects(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	if let Some(arr) = value.as_array() {
		if let Some(k) = args.get("k").and_then(|n| n.as_str()).map(|s| s.to_owned()) {
			let dir = args
				.get("dir")
				.and_then(|d| d.as_str())
				.unwrap_or_else(|| "asc")
				.to_owned();
			let jsonk = format!("/{}", k.replace(".", "/"));

			let mut arr = arr.clone();
			arr.sort_by(|a, b| {
				a.pointer(&jsonk)
					.and_then(|av| av.as_str())
					.and_then(|av| {
						b.pointer(&jsonk).and_then(|bv| bv.as_str()).map(|bv| {
							if dir == "asc" {
								av.cmp(&bv)
							} else {
								bv.cmp(&av)
							}
						})
					})
					.unwrap_or(cmp::Ordering::Equal)
			});

			return Ok(Value::Array(arr));
		}
	}

	Ok(value)
}

fn filter_yaml_encode(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let indent = args.get("indent").and_then(|v| v.as_i64()).unwrap_or_default();
	let indent = " ".repeat(indent as usize);
	Ok(Value::String(
		yaml_encode(&value)
			.unwrap_or_default()
			.lines()
			.skip(1) // Skip the `---` prelude in generated yaml
			.enumerate()
			.map(|(i, l)| {
				if i == 0 {
					format!("{}\n", l.trim_right())
				} else {
					format!("{}{}\n", &indent, l.trim_right())
				}
			})
			.collect::<String>()
			.trim_right_matches('\n')
			.to_string(),
	))
}

fn filter_read_file(value: Value, _: HashMap<String, Value>) -> tera::Result<Value> {
	let res = value
		.as_str()
		.and_then(|p| Some(PathBuf::from(p)))
		.and_then(|p| files::read_to_string(p).ok());

	Ok(Value::String(res.unwrap_or_else(|| "".into())))
}

fn filter_trim(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let trim_chars = args
		.get("c")
		.and_then(|v| v.as_str())
		.unwrap_or(" \t\n\r")
		.chars()
		.collect::<Vec<_>>();
	Ok(Value::String(
		value.as_str().unwrap_or("").trim_matches(&trim_chars[..]).into(),
	))
}

fn filter_trim_left(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let trim_chars = args
		.get("c")
		.and_then(|v| v.as_str())
		.unwrap_or_default()
		.chars()
		.collect::<Vec<_>>();
	Ok(Value::String(
		value.as_str().unwrap_or_default().trim_left_matches(&trim_chars[..]).into(),
	))
}

fn filter_trim_right(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let trim_chars = args
		.get("c")
		.and_then(|v| v.as_str())
		.unwrap_or_default()
		.chars()
		.collect::<Vec<_>>();
	Ok(Value::String(
		value.as_str().unwrap_or_default().trim_right_matches(&trim_chars[..]).into(),
	))
}

fn filter_explode(value: Value, args: HashMap<String, Value>) -> tera::Result<Value> {
	let split = args.get("s").and_then(|v| v.as_str()).unwrap_or(" ");
	Ok(Value::Array(
		value
			.as_str()
			.unwrap_or_default()
			.split(split)
			.map(|v| Value::String(v.to_owned()))
			.collect::<Vec<_>>(),
	))
}

fn tester_starting_with(value: Option<Value>, args: Vec<Value>) -> tera::Result<bool> {
	Ok(value
		.and_then(|v| v.as_str().map(|v| v.to_owned()))
		.and_then(|v| args.first().and_then(|sw| sw.as_str().map(|sw| (v, sw))))
		.map(|(v, sw)| v.starts_with(sw))
		.unwrap_or(false))
}

fn tester_ending_with(value: Option<Value>, args: Vec<Value>) -> tera::Result<bool> {
	Ok(value
		.and_then(|v| v.as_str().map(|v| v.to_owned()))
		.and_then(|v| args.first().and_then(|sw| sw.as_str().map(|sw| (v, sw))))
		.map(|(v, sw)| v.ends_with(sw))
		.unwrap_or(false))
}

fn tester_matching(value: Option<Value>, args: Vec<Value>) -> tera::Result<bool> {
	Ok(value
		.and_then(|v| v.as_str().map(|v| v.to_owned()))
		.and_then(|v| {
			args.first().and_then(|sw| {
				sw.as_str()
					.map(|sw| sw.to_owned())
					.and_then(|sw| Regex::new(&sw).ok())
					.map(|sw| (v, sw))
			})
		})
		.map(|(v, sw)| sw.is_match(&v))
		.unwrap_or(false))
}

fn tester_with_key(value: Option<Value>, args: Vec<Value>) -> tera::Result<bool> {
	Ok(value
		.and_then(|v| v.as_object().cloned())
		.and_then(|v| args.first().and_then(|sw| sw.as_str().map(|sw| (v, sw))))
		.map(|(v, sw)| v.contains_key(sw))
		.unwrap_or(false))
}

fn tester_containing(value: Option<Value>, args: Vec<Value>) -> tera::Result<bool> {
	Ok(value
		.and_then(|v| v.as_array().cloned())
		.and_then(|v| args.first().and_then(|sw| sw.as_str().map(|sw| (v, sw))))
		.map(|(v, sw)| v.contains(&Value::String(sw.to_owned())))
		.unwrap_or(false))
}

fn make_fn_dir_scan() -> tera::GlobalFn {
	Box::new(move |args: HashMap<String, Value>| -> tera::Result<Value> {
		let filter = args
			.get("filter")
			.and_then(|v| json_decode::<String>(v.clone()).ok())
			.and_then(|f| Regex::new(&f).ok())
			.unwrap_or_else(|| Regex::new(".*").unwrap());

		let mut res = Vec::new();

		args.get("dir")
			.and_then(|d| json_decode::<String>(d.clone()).ok())
			.and_then(|d| fs::read_dir(&d).ok())
			.and_then(|paths| -> Option<()> {
				let paths = paths
					.filter_map(|el| el.ok())
					.filter_map(|d| d.path().to_str().map(|s| s.to_string()))
					.filter(|el| filter.is_match(el));

				for file_path in paths {
					res.push(Value::String(file_path.to_string()));
				}
				None
			});

		Ok(Value::Array(res))
	})
}
